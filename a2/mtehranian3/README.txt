* My plotting code is based on the popular Sci-kit Learn libraries, also known
  as the SciPy software stack. The libraries needed to run my code are shown
  below along with the version number I used:
  * Python (2.7)
  * Sci-kit learn (0.19.0)
  * Matplotlib (2.0.2)
  * Pandas (0.20.3)
  * Seaborn (0.8.1)
  * Numpy (1.13.1)
All of these packages can be installed via PIP (Python Package Index)
Instructions taken from: https://www.scipy.org/install.html
  $ python -m pip install --upgrade pip
  $ pip install --user numpy scipy matplotlib ipython jupyter pandas sympy nose
  $ pip install seaborn


* My Optimization code is based on:
  * Jython 2.7.1
  * ABAGAIL/ABAGAIL.jar included
  * Java (1.8.0_73)

How To Run My Code:
* Each section is located in its own folder: digitsNN and optimization.
* Simply change directory to the section folder and run the Jython
  launcher for each model separately.
* You need to use my included ABAGAIL JAR to run the Jython scripts.
* Change the path to the ABAGAIL JAR in each Python script file:
  FROM:
  sys.path.append("/Users/mtehranian/Dev/OMSCS/ml/a2/mtehranian3/ABAGAIL/ABAGAIL.jar")
  TO:
  sys.path.append("/absolute/path/to/the/same/ABAGAIL.jar/file")


  $ cd digitsNN
  $ jython backprop.py
  $ jython rhc.py
  $ jython sa.py
  $ jython ga.py

  $ cd optimization
  $ jython flipflop.py
  $ jython tsp.py
  $ jython continuouspeaks.py


Plotting:
$ cd digitsNN
$ python nn_plotter.py

$ cd optimization
$ python opt_plotter.py


Data Sets:
* Digits: Pulled from the sklearn.datasets library.
  * digitsNN/digits_training.csv
  * digitsNN/digits_validation.csv
  * digitsNN/digits_test.csv
* This data set is already included so you don't have to run the command below
$ cd digitsNN
$ python digits_dumper.py
