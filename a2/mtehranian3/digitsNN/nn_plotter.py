# -*- coding: utf-8 -*-
# data analysis and wrangling
import pandas as pd
import numpy as np
import random as rnd

# visualization
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap

# machine learning
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC, LinearSVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import Perceptron
from sklearn.linear_model import SGDClassifier
from sklearn.tree import DecisionTreeClassifier

import sklearn.model_selection as ms
from sklearn.learning_curve import learning_curve
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler

from sklearn.learning_curve import validation_curve

from sklearn.neural_network import MLPClassifier

from sklearn.metrics import make_scorer, accuracy_score, classification_report, confusion_matrix
from sklearn.utils import compute_sample_weight

from sklearn.datasets import load_digits
from sklearn.cross_validation import train_test_split, cross_val_score


def load_nn_data():
    # Backprop
    training_error = pd.read_csv('./NN_OUTPUT/BACKPROP_LOG.csv')

    # RHC
    #training_error = pd.read_csv('./NN_OUTPUT/RHC_LOG.csv')

    # Simulated Annealing
    #training_error = pd.read_csv('./NN_OUTPUT/SA0.15_LOG.csv')
    #training_error = pd.read_csv('./NN_OUTPUT/SA0.35_LOG.csv')
    #training_error = pd.read_csv('./NN_OUTPUT/SA0.55_LOG.csv')
    #training_error = pd.read_csv('./NN_OUTPUT/SA0.7_LOG.csv')
    #training_error = pd.read_csv('./NN_OUTPUT/SA0.95_LOG.csv')

    # Genetic Algorithm
    #training_error = pd.read_csv('./NN_OUTPUT/GA_50_20_20_LOG.csv')
    #training_error = pd.read_csv('./NN_OUTPUT/GA_50_20_10_LOG.csv')
    #training_error = pd.read_csv('./NN_OUTPUT/GA_50_10_10_LOG.csv')
    #training_error = pd.read_csv('./NN_OUTPUT/GA_50_10_20_LOG.csv')

    # preview the data
    print(training_error.columns.values)
    print training_error.head()
    print training_error.tail()
    print '_'*40
    print training_error.info()
    print '_'*40
    print training_error.describe()

    return training_error


def main():
    data = load_nn_data()
    learning_curve_plot(data)

def learning_curve_plot(train_scores):
    iterations = train_scores['iteration']
    error_training = train_scores['error_training']
    error_validation = train_scores['error_validation']
    error_test = train_scores['error_test']

    #import ipdb; ipdb.set_trace()
    # Training line
    plt.plot(iterations, error_training,
              color='blue', marker='o',
              markersize=5,
              label='Training accuracy')
    # Validation line
    plt.plot(iterations, error_validation,
              color='red', linestyle='--',
              marker='s', markersize=5,
              label='Validation Accuracy')
    # Test line
    plt.plot(iterations, error_test,
              color='green', linestyle='--',
              marker='s', markersize=5,
              label='Test Accuracy')

    plt.grid()
    plt.xlabel('Number of Training Iterations')
    plt.ylabel('Mean-Squared Error')
    plt.yscale('log')
    plt.legend(loc='best')
    plt.title('Backpropagation Optimization Performance', loc='center')
    #plt.ylim([-50.0, error_training.max(axis=0)])
    plt.show()


if __name__ == '__main__':
    main()


