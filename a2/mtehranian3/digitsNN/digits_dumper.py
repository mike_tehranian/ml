# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np

import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap

import sklearn.model_selection as ms

from sklearn.datasets import load_digits

from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_selection import SelectFromModel



digits = load_digits()
X_data = digits.data
Y_data = digits.target
X_train, X_test, Y_train, Y_test = ms.train_test_split(
        X_data,
        Y_data,
        test_size=0.3,
        random_state=0,
        stratify=Y_data)

final_X_train, X_validation, final_Y_train, Y_validation = ms.train_test_split(
        X_train,
        Y_train,
        test_size=0.2,
        random_state=1,
        stratify=Y_train)

#import ipdb; ipdb.set_trace()
final_Y_train_T = np.atleast_2d(final_Y_train).T
Y_validation_T = np.atleast_2d(Y_validation).T
Y_test_T = np.atleast_2d(Y_test).T

training_data = pd.DataFrame(np.hstack((final_X_train,final_Y_train_T)))
validation_data = pd.DataFrame(np.hstack((X_validation,Y_validation_T)))
test_data = pd.DataFrame(np.hstack((X_test,Y_test_T)))

training_data.to_csv('digits_training.csv',index=False,header=False)
validation_data.to_csv('digits_validation.csv',index=False,header=False)
test_data.to_csv('digits_test.csv',index=False,header=False)

