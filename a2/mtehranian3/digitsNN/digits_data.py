# -*- coding: utf-8 -*-
# data analysis and wrangling
import pandas as pd
import numpy as np
import random as rnd

# visualization
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap

# machine learning
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC, LinearSVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import Perceptron
from sklearn.linear_model import SGDClassifier
from sklearn.tree import DecisionTreeClassifier

import sklearn.model_selection as ms
from sklearn.learning_curve import learning_curve
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler

from sklearn.learning_curve import validation_curve


def get_cleansed_data():
    train_df = pd.read_csv('digits_train.csv')
    test_df = pd.read_csv('digits_test.csv')
    combine = [train_df, test_df]
    print(train_df.columns.values)

    # preview the data
    print train_df.head()
    print train_df.tail()
    print '_'*40
    print train_df.info()
    print '_'*40
    print train_df.describe()
    print '_'*40
    print train_df.describe(include=['O'])

    # Analyze data with pivots
    print train_df[['Pclass', 'Survived']] \
            .groupby(['Pclass'], as_index=False) \
            .mean() \
            .sort_values(by='Survived', ascending=False)

    print train_df[["Sex", "Survived"]] \
            .groupby(['Sex'], as_index=False) \
            .mean() \
            .sort_values(by='Survived', ascending=False)

    print train_df[["SibSp", "Survived"]] \
            .groupby(['SibSp'], as_index=False) \
            .mean() \
            .sort_values(by='Survived', ascending=False)

    print train_df[["Parch", "Survived"]] \
            .groupby(['Parch'], as_index=False) \
            .mean() \
            .sort_values(by='Survived', ascending=False)

    # Analyze data with visualizations

    # Age is important
    #g = sns.FacetGrid(train_df, col='Survived')
    #g.map(plt.hist, 'Age', bins=20)

    # Pclass is important
    ## grid = sns.FacetGrid(train_df, col='Pclass', hue='Survived')
    #grid = sns.FacetGrid(train_df, col='Survived', row='Pclass', size=2.2, aspect=1.6)
    #grid.map(plt.hist, 'Age', alpha=.5, bins=20)
    #grid.add_legend()

    # Sex is important
    ## grid = sns.FacetGrid(train_df, col='Embarked')
    #grid = sns.FacetGrid(train_df, row='Embarked', size=2.2, aspect=1.6)
    #grid.map(sns.pointplot, 'Pclass', 'Survived', 'Sex', palette='deep')
    #grid.add_legend()

    # Fare is important
    ## grid = sns.FacetGrid(train_df, col='Embarked', hue='Survived', palette={0: 'k', 1: 'w'})
    #grid = sns.FacetGrid(train_df, row='Embarked', col='Survived', size=2.2, aspect=1.6)
    #grid.map(sns.barplot, 'Sex', 'Fare', alpha=.5, ci=None)
    #grid.add_legend()

    ##plt.show()


    # Drop Ticket and Cabin features
    print ("Before", train_df.shape, test_df.shape, combine[0].shape, combine[1].shape)

    train_df = train_df.drop(['Ticket', 'Cabin'], axis=1)
    test_df = test_df.drop(['Ticket', 'Cabin'], axis=1)
    combine = [train_df, test_df]

    print ("After", train_df.shape, test_df.shape, combine[0].shape, combine[1].shape)

    # Create new features
    for dataset in combine:
        dataset['Title'] = dataset.Name.str.extract(' ([A-Za-z]+)\.', expand=False)

    pd.crosstab(train_df['Title'], train_df['Sex'])

    for dataset in combine:
        dataset['Title'] = dataset['Title'].replace(['Lady', 'Countess','Capt', 'Col',\
                         'Don', 'Dr', 'Major', 'Rev', 'Sir', 'Jonkheer', 'Dona'], 'Rare')

        dataset['Title'] = dataset['Title'].replace('Mlle', 'Miss')
        dataset['Title'] = dataset['Title'].replace('Ms', 'Miss')
        dataset['Title'] = dataset['Title'].replace('Mme', 'Mrs')

    train_df[['Title', 'Survived']] \
        .groupby(['Title'], as_index=False) \
        .mean()

    # Convert Titles to ordinal
    title_mapping = {"Mr": 1, "Miss": 2, "Mrs": 3, "Master": 4, "Rare": 5}
    for dataset in combine:
        dataset['Title'] = dataset['Title'].map(title_mapping)
        dataset['Title'] = dataset['Title'].fillna(0)

    print train_df.head()

    # Drop Name and PassengerId
    train_df = train_df.drop(['Name', 'PassengerId'], axis=1)
    test_df = test_df.drop(['Name'], axis=1)
    combine = [train_df, test_df]
    train_df.shape, test_df.shape

    # Convert Sex to numerical value
    for dataset in combine:
        dataset['Sex'] = dataset['Sex'].map( {'female': 1, 'male': 0} ).astype(int)

    train_df.head()

    # Infer missing values for Age
    ## grid = sns.FacetGrid(train_df, col='Pclass', hue='Gender')
    #grid = sns.FacetGrid(train_df, row='Pclass', col='Sex', size=2.2, aspect=1.6)
    #grid.map(plt.hist, 'Age', alpha=.5, bins=20)
    #grid.add_legend()

    # Populate the missing ages by educated guesses
    guess_ages = np.zeros((2,3))
    print guess_ages

    for dataset in combine:
        for i in range(0, 2):
            for j in range(0, 3):
                guess_df = dataset[(dataset['Sex'] == i) & \
                                      (dataset['Pclass'] == j+1)]['Age'].dropna()

                # age_mean = guess_df.mean()
                # age_std = guess_df.std()
                # age_guess = rnd.uniform(age_mean - age_std, age_mean + age_std)

                age_guess = guess_df.median()

                # Convert random age float to nearest .5 age
                guess_ages[i,j] = int( age_guess/0.5 + 0.5 ) * 0.5

        for i in range(0, 2):
            for j in range(0, 3):
                dataset.loc[ (dataset.Age.isnull()) & (dataset.Sex == i)
                        & (dataset.Pclass == j+1),'Age'] = guess_ages[i,j]

        dataset['Age'] = dataset['Age'].astype(int)

    train_df.head()

    # Create Age Bands
    train_df['AgeBand'] = pd.cut(train_df['Age'], 5)
    train_df[['AgeBand', 'Survived']].groupby(['AgeBand'], as_index=False) \
        .mean() \
        .sort_values(by='AgeBand', ascending=True)

    # Replace Age with ordinal bands
    for dataset in combine:
        dataset.loc[ dataset['Age'] <= 16, 'Age'] = 0
        dataset.loc[(dataset['Age'] > 16) & (dataset['Age'] <= 32), 'Age'] = 1
        dataset.loc[(dataset['Age'] > 32) & (dataset['Age'] <= 48), 'Age'] = 2
        dataset.loc[(dataset['Age'] > 48) & (dataset['Age'] <= 64), 'Age'] = 3
        dataset.loc[ dataset['Age'] > 64, 'Age']
    train_df.head()

    # Drop Age Band
    train_df = train_df.drop(['AgeBand'], axis=1)
    combine = [train_df, test_df]
    train_df.head()


    # Combine features to create Family Size
    for dataset in combine:
        dataset['FamilySize'] = dataset['SibSp'] + dataset['Parch'] + 1

    train_df[['FamilySize', 'Survived']] \
        .groupby(['FamilySize'], as_index=False) \
        .mean() \
        .sort_values(by='Survived', ascending=False)

    # Create a new feature Is Alone
    for dataset in combine:
        dataset['IsAlone'] = 0
        dataset.loc[dataset['FamilySize'] == 1, 'IsAlone'] = 1

    train_df[['IsAlone', 'Survived']].groupby(['IsAlone'], as_index=False).mean()

    # Drop features in favor of Is Alone
    train_df = train_df.drop(['Parch', 'SibSp', 'FamilySize'], axis=1)
    test_df = test_df.drop(['Parch', 'SibSp', 'FamilySize'], axis=1)
    combine = [train_df, test_df]

    train_df.head()

    # Combine Age and Class for new feature
    for dataset in combine:
        dataset['Age*Class'] = dataset.Age * dataset.Pclass

    train_df.loc[:, ['Age*Class', 'Age', 'Pclass']].head(10)

    # Complete the Embarked feature
    freq_port = train_df.Embarked.dropna().mode()[0]

    for dataset in combine:
        dataset['Embarked'] = dataset['Embarked'].fillna(freq_port)

    train_df[['Embarked', 'Survived']] \
        .groupby(['Embarked'], as_index=False) \
        .mean() \
        .sort_values(by='Survived', ascending=False)

    # Convert Embarked to numeric
    for dataset in combine:
        dataset['Embarked'] = dataset['Embarked'].map( {'S': 0, 'C': 1, 'Q': 2} ).astype(int)

    train_df.head()

    # Populate missing Fare values
    test_df['Fare'].fillna(test_df['Fare'].dropna().median(), inplace=True)
    test_df.head()

    train_df['FareBand'] = pd.qcut(train_df['Fare'], 4)
    train_df[['FareBand', 'Survived']] \
        .groupby(['FareBand'], as_index=False) \
        .mean() \
        .sort_values(by='FareBand', ascending=True)

    # Make Fare ordinal value
    for dataset in combine:
        dataset.loc[ dataset['Fare'] <= 7.91, 'Fare'] = 0
        dataset.loc[(dataset['Fare'] > 7.91) & (dataset['Fare'] <= 14.454), 'Fare'] = 1
        dataset.loc[(dataset['Fare'] > 14.454) & (dataset['Fare'] <= 31), 'Fare']   = 2
        dataset.loc[ dataset['Fare'] > 31, 'Fare'] = 3
        dataset['Fare'] = dataset['Fare'].astype(int)

    train_df = train_df.drop(['FareBand'], axis=1)
    combine = [train_df, test_df]

    # Validate test data
    print '$'*40
    print train_df.head(10)

    # Final prepartion for modeling
    X_train = train_df.drop("Survived", axis=1)
    Y_train = train_df["Survived"]
    X_test  = test_df.drop("PassengerId", axis=1).copy()
    print X_train.shape, Y_train.shape, X_test.shape

    # Logistic Regression
    #logreg = LogisticRegression()
    #logreg.fit(X_train, Y_train)
    #Y_pred = logreg.predict(X_test)
    #acc_log = round(logreg.score(X_train, Y_train) * 100, 2)
    #print acc_log

    # Decision Tree
    #decision_tree = DecisionTreeClassifier()
    #decision_tree.fit(X_train, Y_train)
    #Y_pred = decision_tree.predict(X_test)
    #acc_decision_tree = round(decision_tree.score(X_train, Y_train) * 100, 2)
    #print "ONE: " + str(acc_decision_tree)

    # MDT Begin
    # Carve out the train and test from the actual Kaggle training data
    #final_X_train = X_train
    #final_Y_train = Y_train

    final_X_train, final_X_test, final_Y_train, final_Y_test = ms.train_test_split(
            X_train,
            Y_train,
            test_size=0.3,
            random_state=0,
            stratify=Y_train)

    print final_X_train.shape, final_Y_train.shape, final_X_test.shape, final_Y_test.shape

    return final_X_train, final_Y_train, final_X_test, final_Y_test

