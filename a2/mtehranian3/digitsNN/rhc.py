import os
import csv
import time
import sys
sys.path.append("/Users/mtehranian/Dev/OMSCS/ml/a2/mtehranian3/ABAGAIL/ABAGAIL.jar")
from func.nn.backprop import BackPropagationNetworkFactory
from shared import SumOfSquaresError, DataSet, Instance
from opt.example import NeuralNetworkOptimizationProblem
from func.nn.backprop import RPROPUpdateRule, BatchBackPropagationTrainer
import opt.RandomizedHillClimbing as RandomizedHillClimbing
import opt.SimulatedAnnealing as SimulatedAnnealing
import opt.ga.StandardGeneticAlgorithm as StandardGeneticAlgorithm
#from func.nn.activation import RELU
from func.nn.activation import LogisticSigmoid


# Network parameters found "optimal" in Assignment 1
INPUT_LAYER = 64 # Number of input features
HIDDEN_LAYER1 = 128
HIDDEN_LAYER2 = 128
HIDDEN_LAYER3 = 128
OUTPUT_LAYER = 10  # OR 10 # Number of output classifications
TRAINING_ITERATIONS = 5000
OUTFILE = './NN_OUTPUT/RHC_LOG.csv'


def initialize_instances(infile):
    """Read the m_trg.csv CSV data into a list of instances."""
    instances = []

    # Read in the CSV file
    with open(infile, "r") as dat:
        reader = csv.reader(dat)

        for row in reader:
            instance = Instance([float(value) for value in row[:-1]])
            #import pdb; pdb.set_trace()
            label = int(float(row[-1]))
            classes = [0.0] * 10
            classes[label] = 1.0
            instance.setLabel(Instance(classes))

            #instance.setLabel(Instance(float(row[-1])))
            #instance.setLabel(Instance(0 if float(row[-1]) < 0 else 1))

            instances.append(instance)

    return instances


def errorOnDataSet(network,ds,measure):
    N = len(ds)
    error = 0.
    correct = 0
    incorrect = 0
    for instance in ds:
        network.setInputValues(instance.getData())
        network.run()
        actual = instance.getLabel().getContinuous()
        predicted = network.getOutputValues().get(0)
        #import pdb; pdb.set_trace()
        #predicted = max(min(predicted,1),0)
        #if abs(predicted - actual) < 0.5:
            #correct += 1
        #else:
            #incorrect += 1
        output = instance.getLabel()
        #predicted_txt = 'Predicted: {} Actual: {}'.format(predicted, output)
        #print predicted_txt
        output_values = network.getOutputValues()
        mdt = Instance(output_values)
        #import pdb; pdb.set_trace()
        #example = Instance(output_values, Instance(output_values.get(0)))

        label = int(float(output_values.argMax()))
        classes = [0.0] * 10
        classes[label] = 1.0
        #instance.setLabel(Instance(classes))
        #example = Instance(network.getOutputValues());
        #example.setLabel(Instance(Double.parseDouble(network.getOutputValues().toString())));
        #example = Instance(output_values)
        #example.setLabel(Instance(classes))

        #example = Instance(output_values, Instance(classes))
        mdt.setLabel(Instance(classes))

        error += measure.value(output, mdt)
    #MSE = error/float(N)
    #acc = correct/float(correct+incorrect)
    MSE = error
    acc = 0.0
    return MSE,acc


def train(oa, network, oaName, training_ints,validation_ints,testing_ints, measure):
    """Train a given network on a set of instances.
    """
    print "\nError results for %s\n---------------------------" % (oaName,)
    times = [0]
    for iteration in xrange(TRAINING_ITERATIONS):
        start = time.clock()
        oa.train()
        elapsed = time.clock()-start
    	times.append(times[-1]+elapsed)
        if iteration % 20 == 0:
    	    MSE_trg, acc_trg = errorOnDataSet(network,training_ints,measure)
            MSE_val, acc_val = errorOnDataSet(network,validation_ints,measure)
            MSE_tst, acc_tst = errorOnDataSet(network,testing_ints,measure)
            txt = '{},{},{},{},{}\n'.format(iteration,MSE_trg,MSE_val,MSE_tst,times[-1]);
            print txt
            with open(OUTFILE,'a+') as f:
                f.write(txt)


def main():
    """Run this experiment"""
    training_ints = initialize_instances('digits_training.csv')
    testing_ints = initialize_instances('digits_test.csv')
    validation_ints = initialize_instances('digits_validation.csv')
    factory = BackPropagationNetworkFactory()
    measure = SumOfSquaresError()
    data_set = DataSet(training_ints)
    #relu = RELU()
    relu = LogisticSigmoid()
    rule = RPROPUpdateRule()
    oa_names = ["RHC"]
    classification_network = factory.createClassificationNetwork([INPUT_LAYER, HIDDEN_LAYER1,HIDDEN_LAYER2,HIDDEN_LAYER3, OUTPUT_LAYER],relu)
    #classification_network = factory.createClassificationNetwork([INPUT_LAYER, HIDDEN_LAYER1,HIDDEN_LAYER2, OUTPUT_LAYER],relu)
    #train(BatchBackPropagationTrainer(data_set,classification_network,measure,rule), classification_network, 'Backprop', training_ints,validation_ints,testing_ints, measure)

    nnop = NeuralNetworkOptimizationProblem(data_set, classification_network, measure)
    oa = RandomizedHillClimbing(nnop)
    train(oa, classification_network, 'RHC', training_ints,validation_ints,testing_ints, measure)


if __name__ == "__main__":
    with open(OUTFILE,'w') as f:
        f.write('{},{},{},{},{}\n'.format('iteration','error_training','error_validation','error_test','elapsed'))
    main()
