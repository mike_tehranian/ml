# -*- coding: utf-8 -*-
# data analysis and wrangling
import pandas as pd
import numpy as np
import random as rnd

# visualization
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap

# machine learning
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC, LinearSVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import Perceptron
from sklearn.linear_model import SGDClassifier
from sklearn.tree import DecisionTreeClassifier

import sklearn.model_selection as ms
from sklearn.learning_curve import learning_curve
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler

from sklearn.learning_curve import validation_curve

from sklearn.neural_network import MLPClassifier

from sklearn.metrics import make_scorer, accuracy_score, classification_report, confusion_matrix
from sklearn.utils import compute_sample_weight

from sklearn.datasets import load_digits
from sklearn.cross_validation import train_test_split, cross_val_score

import time


def load_nn_data():
    #BEST:training_error = pd.read_csv('./TSP/TSP_MIMIC100_50_0.1_5_LOG.csv')
    #WORST:training_error = pd.read_csv('./TSP/TSP_MIMIC100_50_0.7_5_LOG.csv')
    #training_error = pd.read_csv('./TSP/TSP_RHC_4_LOG.csv')
    #training_error = pd.read_csv('./TSP/TSP_SA0.55_1_LOG.csv')
    #training_error = pd.read_csv('./TSP/TSP_GA100_30_30_4_LOG.csv')

    #training_error = pd.read_csv('./CONTPEAKS/CONTPEAKS_RHC_5_LOG.txt')
    #training_error = pd.read_csv('./CONTPEAKS/CONTPEAKS_SA0.35_2_LOG.txt')
    #training_error = pd.read_csv('./CONTPEAKS/CONTPEAKS_MIMIC100_50_0.9_3_LOG.txt')
    #training_error = pd.read_csv('./CONTPEAKS/CONTPEAKS_GA100_50_10_2_LOG.txt')

    #training_error = pd.read_csv('./FLIPFLOP/FLIPFLOP_RHC_5_LOG.txt')
    #training_error = pd.read_csv('./FLIPFLOP/FLIPFLOP_SA0.55_3_LOG.txt')
    #training_error = pd.read_csv('./FLIPFLOP/FLIPFLOP_MIMIC100_50_0.5_2_LOG.txt')
    training_error = pd.read_csv('./FLIPFLOP/FLIPFLOP_GA100_50_30_2_LOG.txt')



    # preview the data
    print(training_error.columns.values)
    print training_error.head()
    print training_error.tail()
    print '_'*40
    print training_error.info()
    print '_'*40
    print training_error.describe()

    return training_error


def main():
    data = load_nn_data()
    learning_curve_plot(data)

def learning_curve_plot(train_scores):
    iterations = train_scores['iterations']
    fitness = train_scores['fitness']
    time = train_scores['time']
    fevals = train_scores['fevals']

    #import ipdb; ipdb.set_trace()
    # Training line
    plt.plot(iterations, fitness,
              color='blue', marker='o',
              markersize=5,
              label='Fitness')
    # Validation line
    plt.plot(iterations, time,
              color='red', linestyle='--',
              marker='s', markersize=5,
              label='Time (seconds)')
    # Test line
    plt.plot(iterations, fevals,
              color='green', linestyle='--',
              marker='s', markersize=5,
              label='Function Evaluations')

    plt.grid()
    plt.xlabel('Number of Training Iterations')
    #plt.ylabel('Function Evaluation Score')
    plt.yscale('log')
    plt.legend(loc='best')
    #plt.title('Randomized Hill Climbing', loc='center')
    #plt.title('100 Samples 50 Keep 0.5 M', loc='center')
    #plt.title('Simulated Annealing - 0.55 Cooling Rate', loc='center')
    plt.title('GA 50% Mate 30% Mutate', loc='center')
    #plt.ylim([-50.0, error_training.max(axis=0)])
    plt.show()


if __name__ == '__main__':
    main()


