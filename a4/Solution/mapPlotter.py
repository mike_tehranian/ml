# -*- coding: utf-8 -*-
"""
Created on Sat Apr 22 22:57:10 2017

@author: Jon
"""

import pickle as pkl
import matplotlib.pyplot as plt
import numpy as np

# Easy
# infile = './Value Easy Iter 5 Policy Map.pkl'
# infile = './Value Easy Iter 54 Policy Map.pkl'
# infile = './Policy Easy Iter 5 Policy Map.pkl'
# infile = './Policy Easy Iter 20 Policy Map.pkl'
# infile = './QL Q-Learning L0.1 q100.0 E0.3 Easy Iter 50 Policy Map.pkl'  # Gives slightly optimal numbers
# infile = './QL Q-Learning L0.9 q0.0 E0.5 Easy Iter 50 Policy Map.pkl'  # USE THIS ONE

# Easy Files
# infile = './QL Q-Learning L0.9 q0.0 E0.5 Easy Iter 50 Policy Map.pkl'  # USE THIS ONE
# Easy Q-Learning L0.1 q100.0 E0.3.csv
# infile = './QL Q-Learning L0.1 q100.0 E0.3 Easy Iter 50 Policy Map.pkl'
infile = './QL Q-Learning L0.1 q100.0 E0.1 Easy Iter 1000 Policy Map.pkl'


# Hard Files
# infile = './Value Hard Iter 5 Policy Map.pkl'
# infile = './Value Hard Iter 100 Policy Map.pkl'
# infile = './Policy Hard Iter 5 Policy Map.pkl'
# infile = './Policy Hard Iter 100 Policy Map.pkl'
# infile = './QL Q-Learning L0.1 q100.0 E0.3 Hard Iter 50 Policy Map.pkl'
# infile = './QL Q-Learning L0.1 q100.0 E0.3 Hard Iter 3000 Policy Map.pkl'

with open(infile,'rb') as f:
    # arr = pkl.load(f, encoding='latin1')
    arr = pkl.load(f)

# import ipdb; ipdb.set_trace()
lookup = {'None': (0,0),
          '>': (1,0),
        'v': (0,-1),
        '^':(0,1),
        '<':(-1,0)}

n= len(arr)
arr = np.array(arr)
X, Y = np.meshgrid(range(1,n+1), range(1,n+1))
U = X.copy()
V = Y.copy()
for i in range(n):
    for j in range(n):
        U[i,j]=lookup[arr[n-i-1,j]][0]
        V[i,j]=lookup[arr[n-i-1,j]][1]

plt.figure()
#plt.title('Arrows scale with plot width, not view')
Q = plt.quiver(X, Y, U, V,headaxislength=5,pivot='mid',angles='xy', scale_units='xy', scale=1)
plt.xlim((0,n+1))
plt.ylim((0,n+1))
plt.tight_layout()

plt.show()
