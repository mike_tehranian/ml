# -*- coding: utf-8 -*-
# data analysis and wrangling
import pandas as pd
import numpy as np
import random as rnd

# visualization
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap


def plot_time_per_iterations(value, policy, q_learn):
    iterations_value = value['iter']
    iterations_policy = policy['iter']
    iterations_q = q_learn['iter']

    time_value = value['time']
    time_policy = policy['time']
    time_q = q_learn['time']

    # VI - Time
    plt.plot(iterations_value, time_value,
              color='blue', marker='o',
              markersize=5,
              label='Value Iteration')
    # Policy - Time
    plt.plot(iterations_policy, time_policy,
              color='green', linestyle='--',
              marker='s', markersize=5,
              label='Policy Iteration')
    # Q-Learning - Time
    plt.plot(iterations_q, time_q,
              color='red', linestyle=':',
              marker='^', markersize=5,
              label='QL L0.1 q100.0 E0.3 (Best)')

    plt.grid()
    plt.xlabel('Number of Iterations')
    plt.ylabel('Time (s)')
    # plt.yscale('log')
    plt.xscale('log')
    plt.legend(loc='best')
    plt.title('Time per Iteration', loc='center')
    #plt.ylim([-50.0, error_training.max(axis=0)])
    plt.show()


def plot_reward_per_iterations(value, policy, q_learn):
    iterations_value = value['iter']
    iterations_policy = policy['iter']
    iterations_q = q_learn['iter']

    reward_value = value['reward']
    reward_policy = policy['reward']
    reward_q = q_learn['reward']

    # VI - Reward
    plt.plot(iterations_value, reward_value,
              color='blue', marker='o',
              markersize=5,
              label='Value Iteration')
    # Polic - Reward
    plt.plot(iterations_policy, reward_policy,
              color='green', linestyle='--',
              marker='s', markersize=5,
              label='Policy Iteration')
    # Q-Learning - Reward
    plt.plot(iterations_q, reward_q,
              color='red', linestyle=':',
              marker='^', markersize=5,
              label='QL L0.1 q100.0 E0.3 (Best)')

    plt.grid()
    plt.xlabel('Number of Iterations')
    plt.ylabel('Reward')
    # plt.yscale('log')
    plt.xscale('log')
    plt.legend(loc='best')
    plt.title('Reward for Optimal Path per Iteration', loc='center')
    # plt.ylim([-600.0, -290.0])
    plt.show()


def plot_convergence_per_iterations(value, policy, q_learn):
    iterations_value = value['iter']
    iterations_policy = policy['iter']
    iterations_q = q_learn['iter']

    convergence_value = value['convergence']
    convergence_policy = policy['convergence']
    convergence_q = q_learn['convergence']

    # VI - Reward
    plt.plot(iterations_value, convergence_value,
              color='blue', marker='o',
              markersize=5,
              label='Value Iteration')
    # Polic - Reward
    plt.plot(iterations_policy, convergence_policy,
              color='green', linestyle='--',
              marker='s', markersize=5,
              label='Policy Iteration')
    # Q-Learning - Reward
    plt.plot(iterations_q, convergence_q,
              color='red', linestyle=':',
              marker='^', markersize=5,
              # label='Q-Learning (Optimal)')
              label='QL L0.1 q100.0 E0.3 (Best)')

    plt.grid()
    plt.xlabel('Number of Iterations')
    plt.ylabel('Delta of Value Function (Convergence)')
    # plt.yscale('log')
    plt.xscale('log')
    plt.legend(loc='best')
    plt.title('Delta of Value Function per Iteration (Convergence)', loc='center')
    #plt.ylim([-50.0, error_training.max(axis=0)])
    plt.show()


def plot_steps_per_iterations(value, policy, q_learn):
    iterations_value = value['iter']
    iterations_policy = policy['iter']
    iterations_q = q_learn['iter']

    steps_value = value['steps']
    steps_policy = policy['steps']
    steps_q = q_learn['steps']

    # VI - Reward
    plt.plot(iterations_value, steps_value,
              color='blue', marker='o',
              markersize=5,
              label='Value Iteration')
    # Polic - Reward
    plt.plot(iterations_policy, steps_policy,
              color='green', linestyle='--',
              marker='s', markersize=5,
              label='Policy Iteration')
    # Q-Learning - Reward
    plt.plot(iterations_q, steps_q,
              color='red', linestyle=':',
              marker='^', markersize=5,
              label='QL L0.1 q100.0 E0.3 (Best)')

    plt.grid()
    plt.xlabel('Number of Iterations')
    plt.ylabel('Number of Steps (States)')
    # plt.yscale('log')
    plt.xscale('log')
    plt.legend(loc='best')
    plt.title('Number of State Steps per Iteration', loc='center')
    #plt.ylim([-50.0, error_training.max(axis=0)])
    plt.show()



def plot_reward_per_iterations_param(one, two, three, four, five, six):
    iterations_1 = one['iter']
    iterations_2 = two['iter']
    iterations_3 = three['iter']
    iterations_4 = four['iter']
    iterations_5 = five['iter']
    iterations_6 = six['iter']

    reward_1 = one['reward']
    reward_2 = two['reward']
    reward_3 = three['reward']
    reward_4 = four['reward']
    reward_5 = five['reward']
    reward_6 = six['reward']


    plt.plot(iterations_1, reward_1,
              # color='blue', marker='o',
              color='blue', linestyle=':',
              # markersize=5,
              label='QL L0.1 q0.0 E0.1')
    plt.plot(iterations_2, reward_2,
              color='green', linestyle=':',
              # marker='s', markersize=5,
              label='QL L0.1 q0.0 E0.3')
    plt.plot(iterations_3, reward_3,
              color='red', linestyle=':',
              # marker='^', markersize=5,
              label='QL L0.1 q0.0 E0.5')
    plt.plot(iterations_4, reward_4,
              color='y', linestyle=':',
              # marker='^', markersize=5,
              label='QL L0.9 q100.0 E0.1')
    plt.plot(iterations_5, reward_5,
              color='c', linestyle=':',
              # marker='^', markersize=5,
              label='QL L0.9 q100.0 E0.3')
    plt.plot(iterations_6, reward_6,
              color='m', linestyle=':',
              # marker='^', markersize=5,
              label='QL L0.9 q100.0 E0.5')


    plt.grid()
    plt.xlabel('Number of Iterations')
    plt.ylabel('Reward')
    # plt.yscale('log')
    plt.xscale('log')
    plt.legend(loc='best')
    plt.title('Reward for Optimal Path per Iteration', loc='center')
    plt.ylim([-600.0, 50.0])
    plt.show()


def plot_reward_per_iterations_epsilon(four, five, six):
    iterations_4 = four['iter']
    iterations_5 = five['iter']
    iterations_6 = six['iter']

    reward_4 = four['reward']
    reward_5 = five['reward']
    reward_6 = six['reward']

    plt.plot(iterations_4, reward_4,
              color='y', linestyle=':',
              # marker='^', markersize=5,
              label='QL L0.1 q100.0 E0.1')
    plt.plot(iterations_5, reward_5,
              color='c', linestyle=':',
              # marker='^', markersize=5,
              label='QL L0.1 q100.0 E0.3')
    plt.plot(iterations_6, reward_6,
              color='m', linestyle=':',
              # marker='^', markersize=5,
              label='QL L0.1 q100.0 E0.5')

    plt.grid()
    plt.xlabel('Number of Iterations')
    plt.ylabel('Reward')
    # plt.yscale('log')
    plt.xscale('log')
    plt.legend(loc='best')
    plt.title('Reward for Optimal Path per Iteration - Epsilon Tuning', loc='center')
    # plt.ylim([-600.0, 50.0])
    plt.ylim([-6000.0, -500.0])
    plt.show()


def plot_reward_per_iterations_learning(four, five):
    iterations_4 = four['iter']
    iterations_5 = five['iter']

    reward_4 = four['reward']
    reward_5 = five['reward']

    plt.plot(iterations_4, reward_4,
              color='y', linestyle=':',
              # marker='^', markersize=5,
              label='QL L0.1 q100.0 E0.3')
    plt.plot(iterations_5, reward_5,
              color='c', linestyle=':',
              # marker='^', markersize=5,
              label='QL L0.9 q100.0 E0.3')

    plt.grid()
    plt.xlabel('Number of Iterations')
    plt.ylabel('Reward')
    # plt.yscale('log')
    plt.xscale('log')
    plt.legend(loc='best')
    plt.title('Reward for Optimal Path per Iteration - LR Tuning', loc='center')
    # plt.ylim([-600.0, 50.0])
    plt.ylim([-6000.0, -500.0])
    plt.show()


def plot_reward_per_iterations_qinit(four, five, six):
    iterations_4 = four['iter']
    iterations_5 = five['iter']
    iterations_6 = six['iter']

    reward_4 = four['reward']
    reward_5 = five['reward']
    reward_6 = six['reward']

    plt.plot(iterations_4, reward_4,
              color='y', linestyle=':',
              # marker='^', markersize=5,
              label='QL L0.1 q-100.0 E0.3')
    plt.plot(iterations_5, reward_5,
              color='c', linestyle=':',
              # marker='^', markersize=5,
              label='QL L0.1 q0.0 E0.3')
    plt.plot(iterations_6, reward_6,
              color='m', linestyle=':',
              # marker='^', markersize=5,
              label='QL L0.1 q100.0 E0.3')

    plt.grid()
    plt.xlabel('Number of Iterations')
    plt.ylabel('Reward')
    # plt.yscale('log')
    plt.xscale('log')
    plt.legend(loc='best')
    plt.title('Reward for Optimal Path per Iteration - Q init Tuning', loc='center')
    # plt.ylim([-600.0, 50.0])
    plt.ylim([-6000.0, -500.0])
    plt.show()




def load_data():
    # Best performing Q-Learning parameters
    # best_easy_q_file = './Easy Q-Learning L0.1 q100.0 E0.3.csv'

    # Part 1 - Easy
    # easy_value = pd.read_csv('./Easy Value.csv')
    # easy_policy = pd.read_csv('./Easy Policy.csv')
    # easy_q = pd.read_csv(best_easy_q_file)

    # plot_time_per_iterations(easy_value, easy_policy, easy_q)
    # plot_reward_per_iterations(easy_value, easy_policy, easy_q)
    # plot_convergence_per_iterations(easy_value, easy_policy, easy_q)
    # plot_steps_per_iterations(easy_value, easy_policy, easy_q)


    # # Part 2 - Hard
    easy_value = pd.read_csv('./Hard Value.csv')
    easy_policy = pd.read_csv('./Hard Policy.csv')
    best_easy_q_file = './Hard Q-Learning L0.1 q100.0 E0.3.csv'
    easy_q = pd.read_csv(best_easy_q_file)

    plot_time_per_iterations(easy_value, easy_policy, easy_q)
    # plot_reward_per_iterations(easy_value, easy_policy, easy_q)
    # plot_convergence_per_iterations(easy_value, easy_policy, easy_q)
    # plot_steps_per_iterations(easy_value, easy_policy, easy_q)


    # Part 3 - Easy QL Param
    # one = pd.read_csv('./Easy Q-Learning L0.1 q0.0 E0.1.csv')
    # two = pd.read_csv('./Easy Q-Learning L0.1 q0.0 E0.3.csv')
    # three = pd.read_csv('./Easy Q-Learning L0.1 q0.0 E0.5.csv')
    # four = pd.read_csv('./Easy Q-Learning L0.1 q100.0 E0.1.csv')
    # five = pd.read_csv('./Easy Q-Learning L0.1 q100.0 E0.3.csv')
    # six = pd.read_csv('./Easy Q-Learning L0.1 q100.0 E0.5.csv')

    # plot_reward_per_iterations_param(one, two, three, four, five, six)
    # plot_convergence_per_iterations_param(easy_value, easy_policy, easy_q)
    # plot_steps_per_iterations(easy_value, easy_policy, easy_q)

    # QL epsilon value
    # plot_reward_per_iterations_epsilon(four, five, six)

    # QL L value
    # one = pd.read_csv('./Easy Q-Learning L0.1 q100.0 E0.3.csv')
    # two = pd.read_csv('./Easy Q-Learning L0.9 q100.0 E0.3.csv')
    # plot_reward_per_iterations_learning(one, two)

    # QL Q Init value
    # one = pd.read_csv('./Easy Q-Learning L0.1 q-100.0 E0.3.csv')
    # two = pd.read_csv('./Easy Q-Learning L0.1 q0.0 E0.3.csv')
    # three = pd.read_csv('./Easy Q-Learning L0.1 q100.0 E0.3.csv')
    # plot_reward_per_iterations_qinit(one, two, three)


def load_data_hard_param():
    # Part 3 - Easy QL Param
    # Hard Q-Learning L0.1 q100.0 E0.1.csv
    one = pd.read_csv('./Hard Q-Learning L0.1 q100.0 E0.1.csv')
    two = pd.read_csv('./Hard Q-Learning L0.1 q100.0 E0.3.csv')
    three = pd.read_csv('./Hard Q-Learning L0.1 q100.0 E0.5.csv')
    four = pd.read_csv('./Hard Q-Learning L0.1 q100.0 E0.1.csv')
    five = pd.read_csv('./Hard Q-Learning L0.1 q100.0 E0.3.csv')
    six = pd.read_csv('./Hard Q-Learning L0.1 q100.0 E0.5.csv')

    # plot_reward_per_iterations_epsilon(four, five, six)
    # plot_reward_per_iterations_param(one, two, three, four, five, six)
    # plot_convergence_per_iterations_param(easy_value, easy_policy, easy_q)
    # plot_steps_per_iterations(easy_value, easy_policy, easy_q)

    # QL L value
    # one = pd.read_csv('./Hard Q-Learning L0.1 q100.0 E0.3.csv')
    # two = pd.read_csv('./Hard Q-Learning L0.9 q100.0 E0.3.csv')
    # plot_reward_per_iterations_learning(one, two)

    # QL Q Init value
    one = pd.read_csv('./Hard Q-Learning L0.1 q-100.0 E0.3.csv')
    two = pd.read_csv('./Hard Q-Learning L0.1 q0.0 E0.3.csv')
    three = pd.read_csv('./Hard Q-Learning L0.1 q100.0 E0.3.csv')
    plot_reward_per_iterations_qinit(one, two, three)



def main():
    data = load_data()
    # data = load_data_hard_param()


if __name__ == '__main__':
    main()

