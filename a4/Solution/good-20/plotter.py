# -*- coding: utf-8 -*-
# data analysis and wrangling
import pandas as pd
import numpy as np
import random as rnd

# visualization
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap


def plot_time_per_iterations(value, policy, q_learn):
    iterations_value = value['iter']
    iterations_policy = policy['iter']
    iterations_q = q_learn['iter']

    time_value = value['time']
    time_policy = policy['time']
    time_q = q_learn['time']

    # VI - Time
    plt.plot(iterations_value, time_value,
              color='blue', marker='o',
              markersize=5,
              label='Value Iteration')
    # Policy - Time
    plt.plot(iterations_policy, time_policy,
              color='green', linestyle='--',
              marker='s', markersize=5,
              label='Policy Iteration')
    # Q-Learning - Time
    plt.plot(iterations_q, time_q,
              color='red', linestyle=':',
              marker='^', markersize=5,
              label='Q-Learning (Optimal)')

    plt.grid()
    plt.xlabel('Number of Iterations')
    plt.ylabel('Time (s)')
    # plt.yscale('log')
    plt.xscale('log')
    plt.legend(loc='best')
    plt.title('Time per Iteration', loc='center')
    #plt.ylim([-50.0, error_training.max(axis=0)])
    plt.show()


def plot_reward_per_iterations(value, policy, q_learn):
    iterations_value = value['iter']
    iterations_policy = policy['iter']
    iterations_q = q_learn['iter']

    reward_value = value['reward']
    reward_policy = policy['reward']
    reward_q = q_learn['reward']

    # VI - Reward
    plt.plot(iterations_value, reward_value,
              color='blue', marker='o',
              markersize=5,
              label='Value Iteration')
    # Polic - Reward
    plt.plot(iterations_policy, reward_policy,
              color='green', linestyle='--',
              marker='s', markersize=5,
              label='Policy Iteration')
    # Q-Learning - Reward
    plt.plot(iterations_q, reward_q,
              color='red', linestyle=':',
              marker='^', markersize=5,
              label='Q-Learning (Optimal)')

    plt.grid()
    plt.xlabel('Number of Iterations')
    plt.ylabel('Reward')
    # plt.yscale('log')
    plt.xscale('log')
    plt.legend(loc='best')
    plt.title('Reward for Optimal Path per Iteration', loc='center')
    # plt.ylim([-600.0, -290.0])
    plt.show()


def plot_convergence_per_iterations(value, policy, q_learn):
    iterations_value = value['iter']
    iterations_policy = policy['iter']
    iterations_q = q_learn['iter']

    convergence_value = value['convergence']
    convergence_policy = policy['convergence']
    convergence_q = q_learn['convergence']

    # VI - Reward
    plt.plot(iterations_value, convergence_value,
              color='blue', marker='o',
              markersize=5,
              label='Value Iteration')
    # Polic - Reward
    plt.plot(iterations_policy, convergence_policy,
              color='green', linestyle='--',
              marker='s', markersize=5,
              label='Policy Iteration')
    # Q-Learning - Reward
    plt.plot(iterations_q, convergence_q,
              color='red', linestyle=':',
              marker='^', markersize=5,
              label='Q-Learning (Optimal)')

    plt.grid()
    plt.xlabel('Number of Iterations')
    plt.ylabel('Delta of Value Function (Convergence)')
    # plt.yscale('log')
    plt.xscale('log')
    plt.legend(loc='best')
    plt.title('Delta of Value Function per Iteration', loc='center')
    #plt.ylim([-50.0, error_training.max(axis=0)])
    plt.show()


def plot_steps_per_iterations(value, policy, q_learn):
    iterations_value = value['iter']
    iterations_policy = policy['iter']
    iterations_q = q_learn['iter']

    steps_value = value['steps']
    steps_policy = policy['steps']
    steps_q = q_learn['steps']

    # VI - Reward
    plt.plot(iterations_value, steps_value,
              color='blue', marker='o',
              markersize=5,
              label='Value Iteration')
    # Polic - Reward
    plt.plot(iterations_policy, steps_policy,
              color='green', linestyle='--',
              marker='s', markersize=5,
              label='Policy Iteration')
    # Q-Learning - Reward
    plt.plot(iterations_q, steps_q,
              color='red', linestyle=':',
              marker='^', markersize=5,
              label='Q-Learning (Optimal)')

    plt.grid()
    plt.xlabel('Number of Iterations')
    plt.ylabel('Number of Steps (States)')
    # plt.yscale('log')
    plt.xscale('log')
    plt.legend(loc='best')
    plt.title('Number of State Steps per Iteration', loc='center')
    #plt.ylim([-50.0, error_training.max(axis=0)])
    plt.show()





def load_data():
    # Best performing Q-Learning parameters
    best_easy_q_file = './Easy Q-Learning L0.1 q100.0 E0.3.csv'

    # Part 1 - Easy
    # easy_value = pd.read_csv('./Easy Value.csv')
    # easy_policy = pd.read_csv('./Easy Policy.csv')
    # easy_q = pd.read_csv(best_easy_q_file)

    # plot_time_per_iterations(easy_value, easy_policy, easy_q)
    # plot_reward_per_iterations(easy_value, easy_policy, easy_q)
    # plot_convergence_per_iterations(easy_value, easy_policy, easy_q)
    # plot_steps_per_iterations(easy_value, easy_policy, easy_q)


    # Part 2 - Hard
    easy_value = pd.read_csv('./Hard Value.csv')
    easy_policy = pd.read_csv('./Hard Policy.csv')
    # best_easy_q_file = './Hard Q-Learning L0.1 q100.0 E0.1.csv'
    # best_easy_q_file = './Hard Q-Learning L0.1 q0.0 E0.1.csv'
    # best_easy_q_file = './Hard Q-Learning L0.1 q100.0 E0.1.csv'

    best_easy_q_file = './Hard Q-Learning L0.1 q100.0 E0.1.csv'

    easy_q = pd.read_csv(best_easy_q_file)

    plot_time_per_iterations(easy_value, easy_policy, easy_q)
    plot_reward_per_iterations(easy_value, easy_policy, easy_q)
    plot_convergence_per_iterations(easy_value, easy_policy, easy_q)
    plot_steps_per_iterations(easy_value, easy_policy, easy_q)

    best_easy_q_file = './Hard Q-Learning L0.1 q-100.0 E0.5.csv'

    easy_q = pd.read_csv(best_easy_q_file)

    plot_time_per_iterations(easy_value, easy_policy, easy_q)
    plot_reward_per_iterations(easy_value, easy_policy, easy_q)
    plot_convergence_per_iterations(easy_value, easy_policy, easy_q)
    plot_steps_per_iterations(easy_value, easy_policy, easy_q)


def twod_plotter(reduced_data):
    original = reduced_data.copy()

    x_values = np.array(reduced_data['x']).astype(np.int)
    y_values = np.array(reduced_data['y']).astype(np.int)
    Z = np.array(reduced_data['target']).astype(np.int)
    reduced_data = np.column_stack((x_values, y_values))

    # Step size of the mesh. Decrease to increase the quality of the VQ.
    h = .02     # point in the mesh [x_min, x_max]x[y_min, y_max].

    # Plot the decision boundary. For that, we will assign a color to each
    x_min, x_max = reduced_data[:, 0].min() - 1, reduced_data[:, 0].max() + 1
    y_min, y_max = reduced_data[:, 1].min() - 1, reduced_data[:, 1].max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))

    # Obtain labels for each point in mesh. Use last trained model.
    # Z = kmeans.predict(np.c_[xx.ravel(), yy.ravel()])

    # Put the result into a color plot
    # Z = Z.reshape(xx.shape)
    plt.figure(1)
    plt.clf()
    # plt.imshow(Z, interpolation='nearest',
    #            extent=(xx.min(), xx.max(), yy.min(), yy.max()),
    #            cmap=plt.cm.Paired,
    #            aspect='auto', origin='lower')

    for i in range(10):
        # import ipdb; ipdb.set_trace()
        tmp = np.array(original[original['target'] == i]).astype(np.int)
        # classes = np.column_stack((classes, tmp))
        x_values = np.array(tmp[:,1]).astype(np.int)
        y_values = np.array(tmp[:,2]).astype(np.int)
        # plt.plot(x_values, y_values, linestyle="None", markersize=2)
        plt.scatter(x_values, y_values, s=2)


    # plt.plot(reduced_data[:, 0], reduced_data[:, 1], 'b.', markersize=2)
    # Plot the centroids as a white X
    # centroids = kmeans.cluster_centers_
    # plt.scatter(centroids[:, 0], centroids[:, 1],
    #             marker='x', s=169, linewidths=3,
    #             color='w', zorder=10)
    plt.title('RF-reduced data clusters')
    plt.xlim(x_min, x_max)
    plt.ylim(y_min, y_max)
    plt.xticks(())
    plt.yticks(())
    plt.show()

def find_best_rank(data):
    ranked = data.sort_values('rank_test_score')
    top_record = ranked.head(1)
    # import ipdb; ipdb.set_trace()
    # print top_record['param_NN__alpha']
    # print top_record['param_NN__hidden_layer_sizes']
    # print top_record['param_gmm__n_components']
    hidden_layer = top_record['param_NN__hidden_layer_sizes']
    data_filter = data.loc[data['param_NN__hidden_layer_sizes'].isin(hidden_layer)]
    alpha = top_record['param_NN__alpha']
    data_filter = data_filter.loc[data_filter['param_NN__alpha'].isin(alpha)]
    print "Alpha: {}".format(alpha)
    print "NN Layers: {}".format(hidden_layer)

    # data_filter = data.loc[data['param_NN__hidden_layer_sizes'] == hidden_layer]
    # [36 rows x 24 columns]
    #iloc
    return data_filter

def clustering_plot(train_scores):
    # import ipdb; ipdb.set_trace()
    iterations = map(int, map(str,train_scores.columns.values)[1:])
    # GMM
    gmm_training = train_scores.iloc[0].tail(9)
    # KMeans
    kmeans_training = train_scores.iloc[1].tail(9)

    # GMM
    plt.plot(iterations, gmm_training,
              color='blue', marker='o',
              markersize=5,
              label='GMM')
    # KMeans
    plt.plot(iterations, kmeans_training,
              color='green', linestyle='--',
              marker='s', markersize=5,
              label='KMeans')

    plt.grid()
    plt.xlabel('K Number of Components')
    plt.ylabel('AMI Score')
    # plt.yscale('log')
    plt.legend(loc='best')
    plt.title('Clustering - RF', loc='center')
    #plt.ylim([-50.0, error_training.max(axis=0)])
    plt.show()

def learning_curve_plot(train_scores):
    iterations = train_scores['param_gmm__n_components']
    # iterations = train_scores['param_km__n_clusters']
    error_training = train_scores['mean_train_score']
    error_test = train_scores['mean_test_score']
    std_training = train_scores['std_train_score']
    std_test = train_scores['std_train_score']

    # Training line
    plt.plot(iterations, error_training,
              color='blue', marker='o',
              markersize=5,
              label='Training accuracy')
    plt.fill_between(iterations,
                      error_training + std_training,
                      error_training - std_training,
                      alpha=0.15, color='blue')

    # Test line
    plt.plot(iterations, error_test,
              color='green', linestyle='--',
              marker='s', markersize=5,
              label='Test Accuracy')
    plt.fill_between(iterations,
                      error_test + std_test,
                      error_test - std_test,
                      alpha=0.15, color='green')

    plt.grid()
    # plt.xlabel('GMM K Number of Components')
    plt.xlabel('KMeans K Number of Components')
    plt.ylabel('Accuracy')
    # plt.yscale('log')
    plt.legend(loc='best')
    # plt.title('RP ANN:(100,25,100) Alpha:0.001', loc='center')
    # plt.title('RP ANN:(50,50) Alpha:0.1', loc='center')
    plt.title('RF ANN:(50,50) Alpha:0.1', loc='center')
    #plt.ylim([-50.0, error_training.max(axis=0)])
    plt.show()

def pca_eigenvectors(train_scores):
    # import ipdb; ipdb.set_trace()
    # iterations = map(int, map(str,train_scores.columns.values)[1:])
    # # GMM
    # gmm_training = train_scores.iloc[0].tail(9)
    # # KMeans
    # kmeans_training = train_scores.iloc[1].tail(9)

    # GMM
    plt.plot(train_scores['num'], train_scores['var'],
              color='blue', marker='o',
              markersize=5,
              label='Variance')

    plt.grid()
    plt.xlabel('Number of Eigenvalues')
    plt.ylabel('Variance Explained')
    # plt.yscale('log')
    plt.legend(loc='best')
    plt.title('Cumulative Variance of Eigenvalues', loc='center')
    #plt.ylim([-50.0, error_training.max(axis=0)])
    plt.show()

def ica_kurtosis(train_scores):
    import ipdb; ipdb.set_trace()
    # iterations = map(int, map(str,train_scores.columns.values)[1:])
    # # GMM
    # gmm_training = train_scores.iloc[0].tail(9)
    # # KMeans
    # kmeans_training = train_scores.iloc[1].tail(9)

    # GMM
    plt.plot(train_scores['num'], train_scores['var'],
              color='blue', marker='o',
              markersize=5,
              label='Variance')

    plt.grid()
    plt.xlabel('K Number of Components')
    plt.ylabel('Amount of Kurtosis')
    # plt.yscale('log')
    plt.legend(loc='best')
    plt.title('ICA - Kurtosis per Num Components', loc='center')
    #plt.ylim([-50.0, error_training.max(axis=0)])
    plt.show()

def rp_pairwise_dist(train_scores):
    # import ipdb; ipdb.set_trace()
    # iterations = map(int, map(str,train_scores.columns.values)[1:])
    # # GMM
    # gmm_training = train_scores.iloc[0].tail(9)
    # # KMeans
    # kmeans_training = train_scores.iloc[1].tail(9)

    for i in range(1,13):
        n_iter = int(train_scores.iloc[i][0])
        vals = train_scores.iloc[i][1:]

#         # import ipdb; ipdb.set_trace()
#         tmp = np.array(original[original['target'] == i]).astype(np.int)
#         # classes = np.column_stack((classes, tmp))
#         x_values = np.array(tmp[:,1]).astype(np.int)
#         y_values = np.array(tmp[:,2]).astype(np.int)
#         # plt.plot(x_values, y_values, linestyle="None", markersize=2)
#         plt.scatter(x_values, y_values, s=2)

        plt.plot(range(10), vals,
                  marker='o',
                  markersize=5,
                  label='{} Clusters'.format(n_iter))

    plt.grid()
    plt.xlabel('Iteration Number')
    plt.ylabel('Pairwise Distance')
    # plt.yscale('log')
    plt.legend(loc='best')
    plt.title('RP - Accuracy Score per Iterations', loc='center')
    #plt.ylim([-50.0, error_training.max(axis=0)])
    plt.show()


def main():
    data = load_data()

    # clustering_plot(data)



if __name__ == '__main__':
    main()

