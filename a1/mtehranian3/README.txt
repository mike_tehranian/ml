Overview:
tl;dr I have a very straightforward and popular setup

My code is based on the popular Sci-kit Learn libraries, also known
  as the SciPy software stack.
The libraries needed to run my code are shown below along with the version
  number I used:
* Sci-kit learn (0.19.0)
* Matplotlib (2.0.2)
* Pandas (0.20.3)
* Seaborn (0.8.1)
* Numpy (1.13.1)

All of these packages can be installed via PIP (Python Package Index)
Instructions taken from: https://www.scipy.org/install.html
  $ python -m pip install --upgrade pip
  $ pip install --user numpy scipy matplotlib ipython jupyter pandas sympy nose
  $ pip install seaborn


How To Run My Code:
* Each data set model is located in its own folder
* Simply change directory to the data set folder and run the Python
  launcher for each model separately:

$ cd digits
$ python decision_trees.py
$ python knn.py
$ python neural_networks.py
$ python boost.py
$ python svm.py

$ cd titanic
$ python decision_trees.py
$ python knn.py
$ python neural_networks.py
$ python boost.py
$ python svm.py


Data Sets:
* Titanic: data set is located in the titanic/titanic_train.csv
* Digits: Pulled from the sklearn.datasets library
