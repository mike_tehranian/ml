# -*- coding: utf-8 -*-
# data analysis and wrangling
import pandas as pd
import numpy as np
import random as rnd

# visualization
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap

# machine learning
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC, LinearSVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import Perceptron
from sklearn.linear_model import SGDClassifier
from sklearn.tree import DecisionTreeClassifier

import sklearn.model_selection as ms
from sklearn.learning_curve import learning_curve
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler

from sklearn.learning_curve import validation_curve

#from sklearn.ensemble import RandomForestClassifier
#from sklearn.feature_selection import SelectFromModel

from sklearn.neural_network import MLPClassifier

from sklearn.metrics import make_scorer, accuracy_score
from sklearn.utils import compute_sample_weight

import titanic_data
import mlutils
import time

def main():
    final_X_train, final_Y_train, final_X_test, final_Y_test = titanic_data.get_cleansed_data()
    print final_X_train.shape, final_Y_train.shape, final_X_test.shape, final_Y_test.shape

    best_estimator = best_estimator_grid_search(final_X_train, final_Y_train)
    #learning_curve_plot(best_estimator, final_X_train, final_Y_train)
    validation_curve_plot(best_estimator, final_X_train, final_Y_train)
    # Only for ANN:
    # MDT NEED Training time per num of iterations
    training_time_curve_plot(best_estimator, final_X_train, final_Y_train, final_X_test)


def best_estimator_grid_search(X_train, y_train):
    scorer = make_scorer(mlutils.balanced_accuracy)
    pipe_lr = MLPClassifier()

    #alphas = [0.0001, 0.001, 0.01, 0.1, 1.0, 10.0, 100.0, 1000.0]
    d = X_train.shape[1]
    #hiddens = [(h,)*l for l in [1,2,3] for h in [d,d//2,d*2]]
    alphas = [0.1]
    hiddens = [(16,16)]
    params = {
              'activation': ['relu','logistic'],
              'alpha': alphas,
              'hidden_layer_sizes': hiddens
             }

    opt = ms.GridSearchCV(
            estimator=pipe_lr,
            n_jobs=1,param_grid=params,refit=True,cv=5,scoring=scorer)

    gs = opt.fit(X_train, y_train)
    print "Best score: {}".format(gs.best_score_)
    print "Best params: {}".format(gs.best_params_)

    return gs.best_estimator_


def learning_curve_plot(best_estimator, X_train, y_train):
    train_sizes, train_scores, test_scores = \
            learning_curve(estimator=best_estimator,
                           X=X_train,
                           y=y_train,
                           train_sizes=np.linspace(0.1, 1.0, 10),
                           cv=10,
                           n_jobs=1)

    train_mean = np.mean(train_scores, axis=1)
    train_std = np.std(train_scores, axis=1)
    test_mean = np.mean(test_scores, axis=1)
    test_std = np.std(test_scores, axis=1)

    # Training line
    plt.plot(train_sizes, train_mean,
              color='blue', marker='o',
              markersize=5,
              label='Training accuracy')
    plt.fill_between(train_sizes,
                      train_mean + train_std,
                      train_mean - train_std,
                      alpha=0.15, color='blue')

    # Test line
    plt.plot(train_sizes, test_mean,
              color='green', linestyle='--',
              marker='s', markersize=5,
              label='Validation Accuracy')
    plt.fill_between(train_sizes,
                      test_mean + test_std,
                      test_mean - test_std,
                      alpha=0.15, color='green')
    plt.grid()
    plt.xlabel('Number of training samples')
    plt.ylabel('Accuracy')
    plt.legend(loc='best')
    plt.title('Learning Curve', loc='center')
    plt.ylim([0.6, 1.0])
    plt.show()


def validation_curve_plot(best_estimator, X_train, y_train):
    d = X_train.shape[1]
    param_range = [(h,)*l for l in [1,2,3] for h in [d,d//2,d*2]]
    train_scores, test_scores = validation_curve(
                 estimator=best_estimator,
                 X=X_train,
                 y=y_train,
                 param_name='hidden_layer_sizes',
                 param_range=param_range,
                 cv=10)

    train_mean = np.mean(train_scores, axis=1)
    train_std = np.std(train_scores, axis=1)
    test_mean = np.mean(test_scores, axis=1)
    test_std = np.std(test_scores, axis=1)

    foo = [str(i) for i in param_range]
    x_range = np.arange(len(param_range))
    plt.plot(x_range, train_mean,
          color='blue', marker='o',
          markersize=5,
          label='training accuracy')
    plt.fill_between(x_range, train_mean + train_std,
                  train_mean - train_std, alpha=0.15,
                  color='blue')
    plt.plot(x_range, test_mean,
          color='green', linestyle='--',
          marker='s', markersize=5,
          label='validation accuracy')
    plt.fill_between(x_range,
                  test_mean + test_std,
                  test_mean - test_std,
                  alpha=0.15, color='green')
    plt.grid()
    plt.xticks(x_range, foo)
    plt.legend(loc='best')
    plt.xlabel('Hidden Layer Size')
    plt.ylabel('Accuracy')
    plt.ylim([0.6, 1.0])
    plt.title('Validation Curve', loc='center')
    plt.show()


def training_time_curve_plot(best_estimator, X_train, y_train, X_test):
    param_range = [10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000]
    durations = []
    #import ipdb; ipdb.set_trace()
    for iteration in param_range:
        start = time.time()
        best_estimator.max_iter = iteration
        best_estimator.fit(X_train, y_train)
        score = best_estimator.score(X_train, y_train)
        best_estimator.predict(X_test)
        elapsed = time.time() - start #divide here?
        durations.append(elapsed)

    plt.plot(param_range, durations,
          color='blue', marker='o',
          markersize=5,
          label='iteration time (seconds)')
    plt.grid()
    plt.xscale('log')
    plt.legend(loc='best')
    plt.xlabel('Number of Iterations')
    plt.ylabel('Seconds')
    #plt.ylim([0.6, 1.0])
    plt.title('Time / Number of Iterations', loc='center')
    plt.show()


if __name__ == '__main__':
    main()
