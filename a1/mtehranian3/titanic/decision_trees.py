# -*- coding: utf-8 -*-
# data analysis and wrangling
import pandas as pd
import numpy as np

import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap

from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC, LinearSVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import Perceptron
from sklearn.linear_model import SGDClassifier
from sklearn.tree import DecisionTreeClassifier

import sklearn.model_selection as ms
from sklearn.learning_curve import learning_curve
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler

from sklearn.learning_curve import validation_curve

from sklearn.metrics import make_scorer, accuracy_score
from sklearn.utils import compute_sample_weight

import titanic_data
import mlutils

def main():
    final_X_train, final_Y_train, final_X_test, final_Y_test = titanic_data.get_cleansed_data()
    print final_X_train.shape, final_Y_train.shape, final_X_test.shape, final_Y_test.shape

    # Decision Tree Test
    #decision_tree = DecisionTreeClassifier(criterion='entropy',
                        #max_depth=100, random_state=0)
    #decision_tree.fit(final_X_train, final_Y_train)
    #Y_pred = decision_tree.predict(final_X_test)
    #acc_decision_tree = round(decision_tree.score(final_X_train, final_Y_train) * 100, 2)
    #print "TWO: " + str(acc_decision_tree)

    # print final_X_train.shape, final_Y_train.shape, final_X_test.shape, final_Y_test.shape

    best_estimator = best_estimator_grid_search(final_X_train, final_Y_train)
    learning_curve_plot(best_estimator, final_X_train, final_Y_train)
    validation_curve_plot(best_estimator, final_X_train, final_Y_train)

    # decision_tree_plot(final_X_train, final_X_test, final_Y_train, final_Y_test)


def best_estimator_grid_search(X_train, y_train):
    #pipe_lr = Pipeline([
        #('scl', StandardScaler()),
        #('dt', DecisionTreeClassifier())])
    classifier = DecisionTreeClassifier(criterion='entropy', random_state=0)

    depths = range(2,30)
    params = {
              'max_depth': depths
             }

    scorer = make_scorer(mlutils.balanced_accuracy)
    opt = ms.GridSearchCV(
            estimator=classifier,
            n_jobs=1,param_grid=params,refit=True,cv=10,scoring=scorer)

    gs = opt.fit(X_train, y_train)
    print "Best score: {}".format(gs.best_score_)
    print "Best params: {}".format(gs.best_params_)

    return gs.best_estimator_


def learning_curve_plot(best_estimator, X_train, y_train):
    train_sizes, train_scores, test_scores = \
            learning_curve(estimator=best_estimator,
                           X=X_train,
                           y=y_train,
                           train_sizes=np.linspace(0.1, 1.0, 10),
                           cv=10,
                           n_jobs=1)

    train_mean = np.mean(train_scores, axis=1)
    train_std = np.std(train_scores, axis=1)
    test_mean = np.mean(test_scores, axis=1)
    test_std = np.std(test_scores, axis=1)

    # Training line
    plt.plot(train_sizes, train_mean,
              color='blue', marker='o',
              markersize=5,
              label='Training accuracy')
    plt.fill_between(train_sizes,
                      train_mean + train_std,
                      train_mean - train_std,
                      alpha=0.15, color='blue')

    # Test line
    plt.plot(train_sizes, test_mean,
              color='green', linestyle='--',
              marker='s', markersize=5,
              label='Validation Accuracy')
    plt.fill_between(train_sizes,
                      test_mean + test_std,
                      test_mean - test_std,
                      alpha=0.15, color='green')
    plt.grid()
    plt.xlabel('Number of training samples')
    plt.ylabel('Accuracy')
    plt.legend(loc='best')
    plt.title('Learning Curve', loc='center')
    plt.ylim([0.6, 1.0])
    plt.show()


def validation_curve_plot(best_estimator, X_train, y_train):
    param_range = range(2,30)
    train_scores, test_scores = validation_curve(
                 estimator=best_estimator,
                 X=X_train,
                 y=y_train,
                 param_name='max_depth',
                 param_range=param_range,
                 cv=10)

    train_mean = np.mean(train_scores, axis=1)
    train_std = np.std(train_scores, axis=1)
    test_mean = np.mean(test_scores, axis=1)
    test_std = np.std(test_scores, axis=1)

    plt.plot(param_range, train_mean,
          color='blue', marker='o',
          markersize=5,
          label='training accuracy')
    plt.fill_between(param_range, train_mean + train_std,
                  train_mean - train_std, alpha=0.15,
                  color='blue')
    plt.plot(param_range, test_mean,
          color='green', linestyle='--',
          marker='s', markersize=5,
          label='validation accuracy')
    plt.fill_between(param_range,
                  test_mean + test_std,
                  test_mean - test_std,
                  alpha=0.15, color='green')
    plt.grid()
    plt.xscale('log')
    plt.legend(loc='best')
    plt.xlabel('Max Depth')
    plt.ylabel('Accuracy')
    plt.ylim([0.6, 1.0])
    plt.title('Validation Curve', loc='center')
    plt.show()


def decision_tree_plot(X_train, X_test, Y_train, Y_test):
    tree = DecisionTreeClassifier(criterion='entropy',
                        max_depth=3, random_state=0)
    tree.fit(X_train, Y_train)
    X_combined = np.vstack((X_train, X_test))
    Y_combined = np.hstack((Y_train, Y_test))
    #plot_decision_regions(X_combined, Y_combined,
                        #classifier=tree, test_idx=range(105,150))
    plot_decision_regions(X_combined, Y_combined,
                          classifier=tree)
    plt.xlabel('petal length [cm]')
    plt.ylabel('petal width [cm]')
    plt.legend(loc='upper left')
    plt.show()


# From 'Python Machine Learning'
def plot_decision_regions(X, y, classifier,
                    test_idx=None, resolution=0.02):

    # setup marker generator and color map
    markers = ('s', 'x', 'o', '^', 'v')
    colors = ('red', 'blue', 'lightgreen', 'gray', 'cyan')
    cmap = ListedColormap(colors[:len(np.unique(y))])

    # plot the decision surface
    x1_min, x1_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    x2_min, x2_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    xx1, xx2 = np.meshgrid(np.arange(x1_min, x1_max, resolution),
                         np.arange(x2_min, x2_max, resolution))
    Z = classifier.predict(np.array([xx1.ravel(), xx2.ravel()]).T)
    Z = Z.reshape(xx1.shape)
    plt.contourf(xx1, xx2, Z, alpha=0.4, cmap=cmap)
    plt.xlim(xx1.min(), xx1.max())
    plt.ylim(xx2.min(), xx2.max())

    # plot all samples
    for idx, cl in enumerate(np.unique(y)):
        plt.scatter(x=X[y == cl, 0], y=X[y == cl, 1],
                    alpha=0.8, c=cmap(idx),
                    marker=markers[idx], label=cl)

    # highlight test samples
    if test_idx:
        X_test, y_test = X[test_idx, :], y[test_idx]
        plt.scatter(X_test[:, 0], X_test[:, 1], c='',
                alpha=1.0, linewidths=1, marker='o',
                s=55, label='test set')


if __name__ == '__main__':
    main()
