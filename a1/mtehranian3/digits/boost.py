# -*- coding: utf-8 -*-
# data analysis and wrangling
import pandas as pd
import numpy as np

import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap

from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC, LinearSVC
from sklearn.ensemble import AdaBoostClassifier, RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import Perceptron
from sklearn.linear_model import SGDClassifier
from sklearn.tree import DecisionTreeClassifier

import sklearn.model_selection as ms
from sklearn.learning_curve import learning_curve
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler

from sklearn.learning_curve import validation_curve

from sklearn.metrics import make_scorer, accuracy_score
from sklearn.utils import compute_sample_weight

from sklearn.datasets import load_digits
import mlutils


def main():
    digits = load_digits()
    X_train = digits.data
    Y_train = digits.target
    final_X_train, final_X_test, final_Y_train, final_Y_test = ms.train_test_split(
            X_train,
            Y_train,
            test_size=0.3,
            random_state=0,
            stratify=Y_train)

    best_estimator = best_estimator_grid_search(final_X_train, final_Y_train)
    learning_curve_plot(best_estimator, final_X_train, final_Y_train)
    validation_curve_plot(best_estimator, final_X_train, final_Y_train)


def best_estimator_grid_search(X_train, y_train):
    classifier = DecisionTreeClassifier(criterion='entropy', random_state=0)

    depths = range(2,18)
    params = {
              'max_depth': depths
             }

    scorer = make_scorer(mlutils.balanced_accuracy)
    opt = ms.GridSearchCV(
            estimator=classifier,
            param_grid=params,
            cv=10,
            scoring=scorer)

    gs = opt.fit(X_train, y_train)
    print "Best score: {}".format(gs.best_score_)
    print "Best params: {}".format(gs.best_params_)

    opt = gs.best_estimator_

    boost = AdaBoostClassifier(base_estimator=opt)
    estimator_range = range(2,50)
    learning_range = [(1e-1)*10**-0.5,1e-1,(1e-2)*10**-0.5,
            1e-2,(1e-3)*10**-0.5,1e-3,1,2,3,4,5,6,7,8,9,10]
    #learning_range = [1]
    params = {
              'n_estimators': estimator_range,
              'learning_rate': learning_range
             }

    scorer = make_scorer(mlutils.balanced_accuracy)
    opt = ms.GridSearchCV(
            estimator=boost,
            param_grid=params,
            cv=10,
            scoring=scorer)

    gs = opt.fit(X_train, y_train)
    print "Best score: {}".format(gs.best_score_)
    print "Best params: {}".format(gs.best_params_)

    return gs.best_estimator_


def learning_curve_plot(best_estimator, X_train, y_train):
    train_sizes, train_scores, test_scores = \
            learning_curve(estimator=best_estimator,
                           X=X_train,
                           y=y_train,
                           train_sizes=np.linspace(0.1, 1.0, 10),
                           cv=10,
                           n_jobs=1)

    train_mean = np.mean(train_scores, axis=1)
    train_std = np.std(train_scores, axis=1)
    test_mean = np.mean(test_scores, axis=1)
    test_std = np.std(test_scores, axis=1)

    # Training line
    plt.plot(train_sizes, train_mean,
              color='blue', marker='o',
              markersize=5,
              label='Training accuracy')
    plt.fill_between(train_sizes,
                      train_mean + train_std,
                      train_mean - train_std,
                      alpha=0.15, color='blue')

    # Test line
    plt.plot(train_sizes, test_mean,
              color='green', linestyle='--',
              marker='s', markersize=5,
              label='Validation Accuracy')
    plt.fill_between(train_sizes,
                      test_mean + test_std,
                      test_mean - test_std,
                      alpha=0.15, color='green')
    plt.grid()
    plt.xlabel('Number of training samples')
    plt.ylabel('Accuracy')
    plt.legend(loc='best')
    plt.title('Learning Curve', loc='center')
    plt.ylim([0.6, 1.0])
    plt.show()


def validation_curve_plot(best_estimator, X_train, y_train):
    param_range = range(1,200)
    train_scores, test_scores = validation_curve(
                 estimator=best_estimator,
                 X=X_train,
                 y=y_train,
                 param_name='n_estimators',
                 param_range=param_range,
                 cv=10)

    train_mean = np.mean(train_scores, axis=1)
    train_std = np.std(train_scores, axis=1)
    test_mean = np.mean(test_scores, axis=1)
    test_std = np.std(test_scores, axis=1)

    plt.plot(param_range, train_mean,
          color='blue', marker='o',
          markersize=5,
          label='training accuracy')
    plt.fill_between(param_range, train_mean + train_std,
                  train_mean - train_std, alpha=0.15,
                  color='blue')
    plt.plot(param_range, test_mean,
          color='green', linestyle='--',
          marker='s', markersize=5,
          label='validation accuracy')
    plt.fill_between(param_range,
                  test_mean + test_std,
                  test_mean - test_std,
                  alpha=0.15, color='green')
    plt.grid()
    plt.xscale('log')
    plt.legend(loc='best')
    plt.xlabel('Number of Estimators')
    plt.ylabel('Accuracy')
    plt.ylim([0.6, 1.0])
    plt.title('Validation Curve', loc='center')
    plt.show()


if __name__ == '__main__':
    main()
