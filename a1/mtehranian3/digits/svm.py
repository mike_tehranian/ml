# -*- coding: utf-8 -*-
# data analysis and wrangling
import pandas as pd
import numpy as np

import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap

from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC, LinearSVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import Perceptron
from sklearn.linear_model import SGDClassifier
from sklearn.tree import DecisionTreeClassifier

import sklearn.model_selection as ms
from sklearn.learning_curve import learning_curve
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler

from sklearn.learning_curve import validation_curve

from sklearn.metrics import make_scorer, accuracy_score
from sklearn.utils import compute_sample_weight

from sklearn.datasets import load_digits
import mlutils


def main():
    digits = load_digits()
    X_train = digits.data
    Y_train = digits.target
    final_X_train, final_X_test, final_Y_train, final_Y_test = ms.train_test_split(
            X_train,
            Y_train,
            test_size=0.3,
            random_state=0,
            stratify=Y_train)

    best_estimator_rbf = best_estimator_grid_search('rbf', final_X_train, final_Y_train)
    best_estimator_sigmoid = best_estimator_grid_search('sigmoid', final_X_train, final_Y_train)
    learning_curve_plot(best_estimator_rbf,
            best_estimator_sigmoid,
            final_X_train,
            final_Y_train)
    validation_curve_plot(best_estimator_rbf,
            best_estimator_sigmoid,
            final_X_train,
            final_Y_train)


def best_estimator_grid_search(kernel, X_train, y_train):
    pipe_lr = SVC(kernel=kernel)

    alphas = [10**-x for x in np.arange(1,9.01,0.5)]
    num_train = X_train.shape[0]
    #gamma_range_one = np.arange(0.2,2.1,0.2)
    #gamma_range = np.logspace(-6, -1, 5)
    #gamma_range = np.logspace(-6, 2.8, 8)
    gamma_range = np.logspace(-6, 1.01, 8)
    params = {
              'C': gamma_range,
              'gamma': gamma_range,
              #'n_iter': [int((1e6/num_train)/.8) + 1]
             }

    scorer = make_scorer(mlutils.balanced_accuracy)
    opt = ms.GridSearchCV(
            estimator=pipe_lr,
            param_grid=params,
            cv=10,
            scoring=scorer)

    gs = opt.fit(X_train, y_train)
    print "Best score: {}".format(gs.best_score_)
    print "Best params: {}".format(gs.best_params_)

    return gs.best_estimator_


def learning_curve_plot(best_estimator_rbf, best_estimator_sigmoid, X_train, y_train):
    train_sizes, train_scores, test_scores = \
            learning_curve(estimator=best_estimator_rbf,
                           X=X_train,
                           y=y_train,
                           train_sizes=np.linspace(0.1, 1.0, 10),
                           cv=10,
                           n_jobs=1)

    train_mean = np.mean(train_scores, axis=1)
    train_std = np.std(train_scores, axis=1)
    test_mean = np.mean(test_scores, axis=1)
    test_std = np.std(test_scores, axis=1)

    # Training line
    plt.plot(train_sizes, train_mean,
              color='blue', marker='o',
              markersize=5,
              label='Training accuracy (RBF)')
    plt.fill_between(train_sizes,
                      train_mean + train_std,
                      train_mean - train_std,
                      alpha=0.15, color='blue')

    # Test line
    plt.plot(train_sizes, test_mean,
              color='green', linestyle='--',
              marker='s', markersize=5,
              label='Validation Accuracy (RBF)')
    plt.fill_between(train_sizes,
                      test_mean + test_std,
                      test_mean - test_std,
                      alpha=0.15, color='green')

    train_sizes, train_scores, test_scores = \
            learning_curve(estimator=best_estimator_sigmoid,
                           X=X_train,
                           y=y_train,
                           train_sizes=np.linspace(0.1, 1.0, 10),
                           cv=10,
                           n_jobs=1)

    train_mean = np.mean(train_scores, axis=1)
    train_std = np.std(train_scores, axis=1)
    test_mean = np.mean(test_scores, axis=1)
    test_std = np.std(test_scores, axis=1)

    # Training line
    plt.plot(train_sizes, train_mean,
              color='red', marker='o',
              markersize=5,
              label='Training accuracy (Sigmoid)')
    plt.fill_between(train_sizes,
                      train_mean + train_std,
                      train_mean - train_std,
                      alpha=0.15, color='blue')

    # Test line
    plt.plot(train_sizes, test_mean,
              color='yellow', linestyle='--',
              marker='s', markersize=5,
              label='Validation Accuracy (Sigmoid)')
    plt.fill_between(train_sizes,
                      test_mean + test_std,
                      test_mean - test_std,
                      alpha=0.15, color='green')

    plt.grid()
    plt.xlabel('Number of training samples')
    plt.ylabel('Accuracy')
    plt.legend(loc='best')
    plt.title('Learning Curve', loc='center')
    plt.ylim([0.6, 1.0])
    plt.show()


def validation_curve_plot(best_estimator_rbf, best_estimator_sigmoid, X_train, y_train):
    #param_range_one = np.arange(1,51,3)
    #param_range_two = np.logspace(-3, -0.1, 8)
    #param_range = np.concatenate((param_range_two, param_range_one))
    param_range = np.logspace(-6, 1.01, 8)
    train_scores, test_scores = validation_curve(
                 estimator=best_estimator_rbf,
                 X=X_train,
                 y=y_train,
                 param_name='gamma',
                 param_range=param_range,
                 cv=10)

    train_mean = np.mean(train_scores, axis=1)
    train_std = np.std(train_scores, axis=1)
    test_mean = np.mean(test_scores, axis=1)
    test_std = np.std(test_scores, axis=1)

    plt.plot(param_range, train_mean,
          color='blue', marker='o',
          markersize=5,
          label='Training accuracy (RBF)')
    plt.fill_between(param_range, train_mean + train_std,
                  train_mean - train_std, alpha=0.15,
                  color='blue')
    plt.plot(param_range, test_mean,
          color='green', linestyle='--',
          marker='s', markersize=5,
          label='Validation accuracy (RBF)')
    plt.fill_between(param_range,
                  test_mean + test_std,
                  test_mean - test_std,
                  alpha=0.15, color='green')

    param_range = np.logspace(-6, 1.01, 8)
    train_scores, test_scores = validation_curve(
                 estimator=best_estimator_sigmoid,
                 X=X_train,
                 y=y_train,
                 param_name='gamma',
                 param_range=param_range,
                 cv=10)

    train_mean = np.mean(train_scores, axis=1)
    train_std = np.std(train_scores, axis=1)
    test_mean = np.mean(test_scores, axis=1)
    test_std = np.std(test_scores, axis=1)

    plt.plot(param_range, train_mean,
          color='red', marker='o',
          markersize=5,
          label='Training accuracy (Sigmoid)')
    plt.fill_between(param_range, train_mean + train_std,
                  train_mean - train_std, alpha=0.15,
                  color='blue')
    plt.plot(param_range, test_mean,
          color='yellow', linestyle='--',
          marker='s', markersize=5,
          label='Validation accuracy (Sigmoid)')
    plt.fill_between(param_range,
                  test_mean + test_std,
                  test_mean - test_std,
                  alpha=0.15, color='green')

    plt.grid()
    plt.xscale('log')
    plt.legend(loc='best')
    plt.xlabel('Gamma')
    plt.ylabel('Accuracy')
    plt.ylim([0.6, 1.0])
    plt.title('Validation Curve', loc='center')
    plt.show()


if __name__ == '__main__':
    main()
