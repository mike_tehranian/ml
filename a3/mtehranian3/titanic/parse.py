# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
import os
import sklearn.model_selection as ms

import titanic_data

for d in ['BASE','RP','PCA','ICA','RF']:
    n = './{}/'.format(d)
    if not os.path.exists(n):
        os.makedirs(n)

OUT = './BASE/'
# madX1 = pd.read_csv('./madelon_train.data',header=None,sep=' ')
# madX2 = pd.read_csv('./madelon_valid.data',header=None,sep=' ')
# madX = pd.concat([madX1,madX2],0).astype(float)
# madY1 = pd.read_csv('./madelon_train.labels',header=None,sep=' ')
# madY2 = pd.read_csv('./madelon_valid.labels',header=None,sep=' ')
# madY = pd.concat([madY1,madY2],0)
# madY.columns = ['Class']
# madelon_trgX, madelon_tstX, madelon_trgY, madelon_tstY = ms.train_test_split(madX, madY, test_size=0.3, random_state=0,stratify=madY)

final_X_train, final_Y_train, final_X_test, final_Y_test = titanic_data.get_cleansed_data()
# print final_X_train.shape, final_Y_train.shape, final_X_test.shape, final_Y_test.shape

madX = pd.DataFrame(final_X_train)
madY = pd.DataFrame(final_Y_train)
madY.columns = ['Class']

madX2 = pd.DataFrame(final_X_test)
madY2 = pd.DataFrame(final_Y_test)
madY2.columns = ['Class']

mad1 = pd.concat([madX,madY],1)
mad1 = mad1.dropna(axis=1,how='all')
mad1.to_hdf(OUT+'datasets.hdf','digits',complib='blosc',complevel=9)

mad2 = pd.concat([madX2,madY2],1)
mad2 = mad2.dropna(axis=1,how='all')
mad2.to_hdf(OUT+'datasets.hdf','digits_test',complib='blosc',complevel=9)

