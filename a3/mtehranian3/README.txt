Overview:
tl;dr I have a very straightforward and popular setup

My code is based on the popular Sci-kit Learn libraries, also known
  as the SciPy software stack.
The libraries needed to run my code are shown below along with the version
  number I used:
* Python 2.7
* Sci-kit learn (0.19.0)
* Matplotlib (2.0.2)
* Pandas (0.20.3)
* Seaborn (0.8.1)
* Numpy (1.13.1)

All of these packages can be installed via PIP (Python Package Index)
Instructions taken from: https://www.scipy.org/install.html
  $ python -m pip install --upgrade pip
  $ pip install --user numpy scipy matplotlib ipython jupyter pandas sympy nose
  $ pip install seaborn


How To Run My Code:
* Each data set model is located in its own folder
* Simply change directory to the data set folder and run the Python
  launcher for each model separately:
* Plotting is managed in nn_plotter.py
  * Data file to plot is selected in load_nn_data()

$ cd digits
$ python digits_data.py
$ python benchmark.py
$ python clustering.py BASE
$ python ICA.py
$ python PCA.py
$ python RP.py
$ python RF.py
$ python clustering.py ICA
$ python clustering.py PCA
$ python clustering.py RP
$ python clustering.py RF

$ cd titanic
$ python parse.py
$ python benchmark.py
$ python clustering.py BASE
$ python ICA.py
$ python PCA.py
$ python RF.py
$ python RP.py
$ python clustering.py ICA
$ python clustering.py PCA
$ python clustering.py RF
$ python clustering.py RP

Plotting:
$ cd digits
$ python nn_plotter.py

$ cd titanic
$ python nn_plotter.py

Data Sets:
* Titanic: data set is located in the titanic/titanic_train.csv
* Digits: Pulled from the sklearn.datasets library
