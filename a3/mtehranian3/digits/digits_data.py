# -*- coding: utf-8 -*-
# data analysis and wrangling
import pandas as pd
import numpy as np
import os
import random as rnd

# visualization
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap

# machine learning
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC, LinearSVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import Perceptron
from sklearn.linear_model import SGDClassifier
from sklearn.tree import DecisionTreeClassifier

import sklearn.model_selection as ms
from sklearn.learning_curve import learning_curve
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler

from sklearn.learning_curve import validation_curve
from sklearn.datasets import load_digits

#from sklearn.ensemble import RandomForestClassifier
#from sklearn.feature_selection import SelectFromModel


OUT = './BASE/'
for d in ['BASE','RP','PCA','ICA','RF']:
    n = './{}/'.format(d)
    if not os.path.exists(n):
        os.makedirs(n)

# def write_digits_data():
    # train_df = pd.read_csv('digits_train.csv')
    # test_df = pd.read_csv('digits_test.csv')
    # combine = [train_df, test_df]
    # print(train_df.columns.values)

    # final_X_train, final_X_test, final_Y_train, final_Y_test = ms.train_test_split(
    #         X_train,
    #         Y_train,
    #         test_size=0.3,
    #         random_state=0,
    #         stratify=Y_train)

    # print final_X_train.shape, final_Y_train.shape, final_X_test.shape, final_Y_test.shape

    # return final_X_train, final_Y_train, final_X_test, final_Y_test

digits = load_digits(return_X_y=True)
digitsX, digitsY = digits

digits = np.hstack((digitsX, np.atleast_2d(digitsY).T))
digits = pd.DataFrame(digits)
cols = list(range(digits.shape[1]))
cols[-1] = 'Class'
digits.columns = cols
digits.to_hdf(OUT+'datasets.hdf','digits',complib='blosc',complevel=9)
