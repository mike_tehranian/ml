# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np

from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import GridSearchCV
from helpers import   nn_arch,nn_reg

out = './BASE/'
np.random.seed(0)
digits = pd.read_hdf('./BASE/datasets.hdf','digits')
digitsX = digits.drop('Class',1).copy().values
digitsY = digits['Class'].copy().values

digitsX= StandardScaler().fit_transform(digitsX)

#%% benchmarking for chart type 2

# Optimal Parameters Found from Assignment 1
# nn_arch= [(128,128,128)]
# nn_reg = [0.0001]
# nn_arch= [(50,50),(50,),(25,),(25,25),(100,25,100),(128,128,128)]
# nn_reg = [10**-x for x in range(1,5)]

grid ={'NN__alpha':nn_reg,'NN__hidden_layer_sizes':nn_arch}
mlp = MLPClassifier(activation='relu',max_iter=2000,early_stopping=True,random_state=5,hidden_layer_sizes=(128,128,128),alpha=0.0001)

pipe = Pipeline([('NN',mlp)])
gs = GridSearchCV(pipe,grid,verbose=10,cv=5)

gs.fit(digitsX,digitsY)
tmp = pd.DataFrame(gs.cv_results_)
tmp.to_csv(out+'digits_NN_bmk.csv')
