# -*- coding: utf-8 -*-
# data analysis and wrangling
import pandas as pd
import numpy as np
import random as rnd

# visualization
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap

# machine learning
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC, LinearSVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import Perceptron
from sklearn.linear_model import SGDClassifier
from sklearn.tree import DecisionTreeClassifier

import sklearn.model_selection as ms
from sklearn.learning_curve import learning_curve
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler

from sklearn.learning_curve import validation_curve

from sklearn.neural_network import MLPClassifier

from sklearn.metrics import make_scorer, accuracy_score, classification_report, confusion_matrix
from sklearn.utils import compute_sample_weight

from sklearn.datasets import load_digits
from sklearn.cross_validation import train_test_split, cross_val_score


def load_nn_data():
    # Part 1 - Clustering
    # training_error = pd.read_csv('./BASE/SSE.csv')
    # training_error = pd.read_csv('./BASE/logliklihood.csv')
    # training_error = pd.read_csv('./BASE/Digits_acc.csv')
    # training_error = pd.read_csv('./BASE/Digits_adjMI.csv')
    # training_error = pd.read_csv('./BASE/Digits_cluster_Kmeans.csv')
    # training_error = pd.read_csv('./BASE/Digits_cluster_GMM.csv')
    # training_error = pd.read_csv('./BASE/digits2D.csv')

    # Part 2 - DR
    # training_error = pd.read_csv('./ICA/digits_scree.csv')
    # training_error = pd.read_csv('./PCA/digits_scree.csv')
    # training_error = pd.read_csv('./RF/digits_scree.csv')
    training_error = pd.read_csv('./RP/digits_scree1.csv')
    # training_error = pd.read_csv('./RP/digits_scree2.csv')
    # training_error = pd.read_csv('./ICA/digits2D.csv')
    # training_error = pd.read_csv('./PCA/digits2D.csv')
    # training_error = pd.read_csv('./RP/digits2D.csv')
    # training_error = pd.read_csv('./RF/digits2D.csv')

    # Part 3 - DR -> Clustering
    # training_error = pd.read_csv('./ICA/SSE.csv')
    # training_error = pd.read_csv('./ICA/logliklihood.csv')
    # training_error = pd.read_csv('./ICA/Digits_acc.csv')
    # training_error = pd.read_csv('./ICA/Digits_adjMI.csv')
    # training_error = pd.read_csv('./PCA/Digits_adjMI.csv')
    # training_error = pd.read_csv('./RP/Digits_adjMI.csv')
    # training_error = pd.read_csv('./RF/Digits_adjMI.csv')

    # Part 4 - DR -> ANN
    # training_error = pd.read_csv('./ICA/digits_dim_red.csv')
    # training_error = pd.read_csv('./PCA/digits_dim_red.csv')
    # training_error = pd.read_csv('./RF/digits_dim_red.csv')
    # training_error = pd.read_csv('./RP/digits_dim_red.csv')

    # Part 5 - DR -> Clustering -> ANN
    # training_error = pd.read_csv('./ICA/Digits_cluster_GMM.csv')
    # training_error = pd.read_csv('./ICA/Digits_cluster_Kmeans.csv')
    # training_error = pd.read_csv('./PCA/Digits_cluster_GMM.csv')
    # training_error = pd.read_csv('./PCA/Digits_cluster_Kmeans.csv')
    # training_error = pd.read_csv('./RF/Digits_cluster_GMM.csv')
    # training_error = pd.read_csv('./RF/Digits_cluster_Kmeans.csv')
    # training_error = pd.read_csv('./RP/Digits_cluster_GMM.csv')
    # training_error = pd.read_csv('./RP/Digits_cluster_Kmeans.csv')

    # preview the data
    # print(training_error.values)
    # print training_error.head()
    # print training_error.tail()
    # print '_'*40
    # print training_error.info()
    # print '_'*40
    # print training_error.describe()

    return training_error

def twod_plotter(reduced_data):
    original = reduced_data.copy()

    x_values = np.array(reduced_data['x']).astype(np.int)
    y_values = np.array(reduced_data['y']).astype(np.int)
    Z = np.array(reduced_data['target']).astype(np.int)
    reduced_data = np.column_stack((x_values, y_values))

    # Step size of the mesh. Decrease to increase the quality of the VQ.
    h = .02     # point in the mesh [x_min, x_max]x[y_min, y_max].

    # Plot the decision boundary. For that, we will assign a color to each
    x_min, x_max = reduced_data[:, 0].min() - 1, reduced_data[:, 0].max() + 1
    y_min, y_max = reduced_data[:, 1].min() - 1, reduced_data[:, 1].max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))

    # Obtain labels for each point in mesh. Use last trained model.
    # Z = kmeans.predict(np.c_[xx.ravel(), yy.ravel()])

    # Put the result into a color plot
    # Z = Z.reshape(xx.shape)
    plt.figure(1)
    plt.clf()
    # plt.imshow(Z, interpolation='nearest',
    #            extent=(xx.min(), xx.max(), yy.min(), yy.max()),
    #            cmap=plt.cm.Paired,
    #            aspect='auto', origin='lower')

    for i in range(10):
        # import ipdb; ipdb.set_trace()
        tmp = np.array(original[original['target'] == i]).astype(np.int)
        # classes = np.column_stack((classes, tmp))
        x_values = np.array(tmp[:,1]).astype(np.int)
        y_values = np.array(tmp[:,2]).astype(np.int)
        # plt.plot(x_values, y_values, linestyle="None", markersize=2)
        plt.scatter(x_values, y_values, s=2)


    # plt.plot(reduced_data[:, 0], reduced_data[:, 1], 'b.', markersize=2)
    # Plot the centroids as a white X
    # centroids = kmeans.cluster_centers_
    # plt.scatter(centroids[:, 0], centroids[:, 1],
    #             marker='x', s=169, linewidths=3,
    #             color='w', zorder=10)
    plt.title('RF-reduced data clusters')
    plt.xlim(x_min, x_max)
    plt.ylim(y_min, y_max)
    plt.xticks(())
    plt.yticks(())
    plt.show()

def find_best_rank(data):
    ranked = data.sort_values('rank_test_score')
    top_record = ranked.head(1)
    # import ipdb; ipdb.set_trace()
    # print top_record['param_NN__alpha']
    # print top_record['param_NN__hidden_layer_sizes']
    # print top_record['param_gmm__n_components']
    hidden_layer = top_record['param_NN__hidden_layer_sizes']
    data_filter = data.loc[data['param_NN__hidden_layer_sizes'].isin(hidden_layer)]
    alpha = top_record['param_NN__alpha']
    data_filter = data_filter.loc[data_filter['param_NN__alpha'].isin(alpha)]
    print "Alpha: {}".format(alpha)
    print "NN Layers: {}".format(hidden_layer)

    # data_filter = data.loc[data['param_NN__hidden_layer_sizes'] == hidden_layer]
    # [36 rows x 24 columns]
    #iloc
    return data_filter

def clustering_plot(train_scores):
    # import ipdb; ipdb.set_trace()
    iterations = map(int, map(str,train_scores.columns.values)[1:])
    # GMM
    gmm_training = train_scores.iloc[0].tail(9)
    # KMeans
    kmeans_training = train_scores.iloc[1].tail(9)

    # GMM
    plt.plot(iterations, gmm_training,
              color='blue', marker='o',
              markersize=5,
              label='GMM')
    # KMeans
    plt.plot(iterations, kmeans_training,
              color='green', linestyle='--',
              marker='s', markersize=5,
              label='KMeans')

    plt.grid()
    plt.xlabel('K Number of Components')
    plt.ylabel('AMI Score')
    # plt.yscale('log')
    plt.legend(loc='best')
    plt.title('Clustering - RF', loc='center')
    #plt.ylim([-50.0, error_training.max(axis=0)])
    plt.show()

def learning_curve_plot(train_scores):
    iterations = train_scores['param_gmm__n_components']
    # iterations = train_scores['param_km__n_clusters']
    error_training = train_scores['mean_train_score']
    error_test = train_scores['mean_test_score']
    std_training = train_scores['std_train_score']
    std_test = train_scores['std_train_score']

    # Training line
    plt.plot(iterations, error_training,
              color='blue', marker='o',
              markersize=5,
              label='Training accuracy')
    plt.fill_between(iterations,
                      error_training + std_training,
                      error_training - std_training,
                      alpha=0.15, color='blue')

    # Test line
    plt.plot(iterations, error_test,
              color='green', linestyle='--',
              marker='s', markersize=5,
              label='Test Accuracy')
    plt.fill_between(iterations,
                      error_test + std_test,
                      error_test - std_test,
                      alpha=0.15, color='green')

    plt.grid()
    # plt.xlabel('GMM K Number of Components')
    plt.xlabel('KMeans K Number of Components')
    plt.ylabel('Accuracy')
    # plt.yscale('log')
    plt.legend(loc='best')
    # plt.title('RP ANN:(100,25,100) Alpha:0.001', loc='center')
    # plt.title('RP ANN:(50,50) Alpha:0.1', loc='center')
    plt.title('RF ANN:(50,50) Alpha:0.1', loc='center')
    #plt.ylim([-50.0, error_training.max(axis=0)])
    plt.show()

def pca_eigenvectors(train_scores):
    import ipdb; ipdb.set_trace()
    # iterations = map(int, map(str,train_scores.columns.values)[1:])
    # # GMM
    # gmm_training = train_scores.iloc[0].tail(9)
    # # KMeans
    # kmeans_training = train_scores.iloc[1].tail(9)

    # GMM
    plt.plot(train_scores['num'], train_scores['var'],
              color='blue', marker='o',
              markersize=5,
              label='Variance')

    plt.grid()
    plt.xlabel('Number of Eigenvalues')
    plt.ylabel('Variance Explained')
    # plt.yscale('log')
    plt.legend(loc='best')
    plt.title('Cumulative Variance of Eigenvalues', loc='center')
    #plt.ylim([-50.0, error_training.max(axis=0)])
    plt.show()

def ica_kurtosis(train_scores):
    import ipdb; ipdb.set_trace()
    # iterations = map(int, map(str,train_scores.columns.values)[1:])
    # # GMM
    # gmm_training = train_scores.iloc[0].tail(9)
    # # KMeans
    # kmeans_training = train_scores.iloc[1].tail(9)

    # GMM
    plt.plot(train_scores['num'], train_scores['var'],
              color='blue', marker='o',
              markersize=5,
              label='Variance')

    plt.grid()
    plt.xlabel('K Number of Components')
    plt.ylabel('Amount of Kurtosis')
    # plt.yscale('log')
    plt.legend(loc='best')
    plt.title('ICA - Kurtosis per Num Components', loc='center')
    #plt.ylim([-50.0, error_training.max(axis=0)])
    plt.show()

def rp_pairwise_dist(train_scores):
    # import ipdb; ipdb.set_trace()
    # iterations = map(int, map(str,train_scores.columns.values)[1:])
    # # GMM
    # gmm_training = train_scores.iloc[0].tail(9)
    # # KMeans
    # kmeans_training = train_scores.iloc[1].tail(9)

    for i in range(1,13):
        n_iter = int(train_scores.iloc[i][0])
        vals = train_scores.iloc[i][1:]

#         # import ipdb; ipdb.set_trace()
#         tmp = np.array(original[original['target'] == i]).astype(np.int)
#         # classes = np.column_stack((classes, tmp))
#         x_values = np.array(tmp[:,1]).astype(np.int)
#         y_values = np.array(tmp[:,2]).astype(np.int)
#         # plt.plot(x_values, y_values, linestyle="None", markersize=2)
#         plt.scatter(x_values, y_values, s=2)

        plt.plot(range(10), vals,
                  marker='o',
                  markersize=5,
                  label='{} Clusters'.format(n_iter))

    plt.grid()
    plt.xlabel('Iteration Number')
    plt.ylabel('Pairwise Distance')
    # plt.yscale('log')
    plt.legend(loc='best')
    plt.title('RP - Accuracy Score per Iterations', loc='center')
    #plt.ylim([-50.0, error_training.max(axis=0)])
    plt.show()


def main():
    data = load_nn_data()

    # clustering_plot(data)

    # data_filter = find_best_rank(data)
    # learning_curve_plot(data_filter)

    # twod_plotter(data)

    # pca_eigenvectors(data)

    # ica_kurtosis(data)

    rp_pairwise_dist(data)

if __name__ == '__main__':
    main()
